NB : voir les autres fichiers NOM.md en racine pour des infos spécifiques

# Pour préparer une mise en prod
Penser à pusher les 3 dossiers git du projet :
- le projet racine
- le sous-dossier privé des /resources/views/memos/
- le sous-dossier privé des /public/img/figs/memos/
Puis récupérer les commandes SQL pour intégrer les nouveaux fichiers

# Premier déploiement pour tests en local
1. Créer une DB, son user SQL et lui donner les droits sur la DB
2. Dupliquer le .env.example en .env et le compléter
```
php artisan key:generate
```

3. Récupérer toutes les bibliothèques requises et construire les tables :
```
composer install
npm install
npm run dev
php artisan migrate
```

1. Lancer le fonctionnement du serveur de tests :
`php artisan serve`

1. Ouvrir le fichier routes/auth.php et si besoin décommenter les requêtes GET et POST pour l'inscription (commenter la GET qui annonce que les inscriptions sont fermées)

2. Se rendre sur http://127.0.0.1:8000/register et créer le premier user via l'interface proposée

3. Peupler la DB en faisant :
`php artisan db:seed`


# Commandes utiles
* lancer le serveur de test (si pas de nginx prévu)
`php artisan serve`

* Rejouer toutes les migrations pour repartir d'une DB vierge et propre :
```
php artisan migrate:refresh
php artisan db:seed
```

* Peupler une table grâce à des seeds précis :
`php artisan db:seed --class=MemoSeeder`