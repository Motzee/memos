# Premier déploiement sur serveur
1. Cloner le projet
2. Créer un sous-domaine qui pointera sur son dossier public/
3. composer install
4. Créer une DB et un user sur le serveur
5. copie le .env.example et le modifier avec les bonnes infos (laisser le mode dev temporairement)
6. générer une clef de projet :
`php artisan key:generate`
7. `npm install` puis `npm run dev` ou bien se connecter en ftp et copier le dossier des node_modules
8. Lancer la création des tables via `php artisan migrate`  (il peut être nécessaire d'aller commenter temporairement le __construct de MemosFromFolder, je sais pas pourquoi mais l'absence de l'une des tables le chafouine alors qu'il est pas censé intervenir à cette étape)
9. Se connecter sur la page /register via le navigateur puis créer le premier utilisateur (si la page ne s'affiche pas, vérifier qu'il y a bien un .htaccess dans le dossier public/ afin de gérer le routing ; c'est pareil que pour les projets Symfony quoi)
10. Modifier à nouveau le .env pour repasser en mode prod et ainsi fermer les inscriptions
`php artisan cache:clear config:clear route:clear`
11. Lancer la création des domaines et sous-domaines
    php artisan domaines:import_from_json
    
12. Lancer le remplissage de la DB via les seeds : `php artisan db:seed`

# Lors d'updates
3. composer install
7. `npm install` puis `npm run dev` ou bien se connecter en ftp et copier le dossier des node_modules
8. Lancer la création des tables via `php artisan migrate`