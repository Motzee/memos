<?php

namespace App\Services;

class ScandirService {



    /* ATTENTION : on est vraiment sur de la vérification de base : si l'extension est absente ou si le fichier a la mauvaise extension, la fonction se laisse berner */
    public static function getFilesOfTypeInFolder(string $extension, string $folderPath) {
        $folderContent = scandir($folderPath);

        $purgedFolderContent = array_filter($folderContent, function ($entry)use ($extension, $folderPath) {
            if ($entry == "." 
                || $entry == ".." 
                || $entry === ".git" 
                || is_dir($folderPath . DIRECTORY_SEPARATOR . $entry)) {
                return false;
            }

            $ext = pathinfo($entry, PATHINFO_EXTENSION);
            return $ext === $extension;
        });

        return $purgedFolderContent ;
    }
}