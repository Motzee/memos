<?php

namespace App\Imports;

use App\Enums\LanguageWordEnum;
use App\Models\Vocabword;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VocabulaireImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!isset($row['motfr']) || empty($row['motfr'])) {
            return ;
        }
        //pour chaque ligne on va :

        /* créer une entrée en français */
        $wordFR = new Vocabword([
            'lang'          => LanguageWordEnum::FR,
            'word'          => $row['motfr'],
            'gender'        => $row['genrefr'],
            'class'         => $row['classegrammaticale'],
            'picture'       => $row['vignette'],
            'details'       => null,
            'level'         => $row['niveau'],
            'is_public'     => 1
        ]) ;
        $wordFR->save();

        // dd($wordFR->id);
        /* créer une entrée correspondante à chaque langue renseignée */
        if(trim($row['moten']) !== '') {
            $this->createTranslation($wordFR->id, LanguageWordEnum::EN, $row['moten'], $row['genreen'], $row['classegrammaticale'], $row['vignette'], $row['niveau'], null) ;
        }
        
        if(trim($row['motde']) !== '') {
            $this->createTranslation($wordFR->id, LanguageWordEnum::DE, $row['motde'], $row['genrede'], $row['classegrammaticale'], $row['vignette'], $row['niveau'], null) ;
        }

        //associer chaque mot à son/ses thèmes
        //TODO
        
        return $wordFR ;
    }

    protected function createTranslation($wordFrId, $lang, $word, $gender, $class, $picture, $level, $details = null) {
        $translation = new Vocabword([
            'id_equivalent_fr'  => $wordFrId,
            'lang'              => $lang,
            'word'              => $word,
            'gender'            => $gender,
            'class'             => $class,
            'picture'           => $picture,
            'details'           => $details,
            'level'             => $level,
            'is_public'         => 1
        ]);
        $translation->save();
    }

    
    public function headingRow(): int
    {
        return 1;
    }
}
