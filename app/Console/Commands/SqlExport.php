<?php

namespace App\Console\Commands;

use DateTimeZone;
use Illuminate\Support\Str;
use App\Services\BddService;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Spatie\DbDumper\Databases\MySql;

class SqlExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bdd:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Exporte des tables de la base de données pour qu'elle puisse être réimportée ailleurs";

    protected $tablesToExport ;
    /* -------------------------------------------------------------------------------*/

    

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->tablesToExport = BddService::TABLES_TO_EXPORT ;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $nowFormatted = Carbon::now(new DateTimeZone('Europe/Paris'))->format("Y-m-d_H-i-s");
        $filename = 'memos_'.$nowFormatted.'.sql' ;
        $memosPath = 'resources'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'memos' ;

        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ") ;
        Log::debug("Début de l'export de la BDD ".env('DB_DATABASE')." via l'user ". env('DB_USERNAME')) ;

        //TODO voir comment gérer les users afin que leur mot de passe (même hashé) ne soit pas stocké dans le fichier. Pour l'user admin, on pourra effacer son pass ici, puis se servir des variables d'env dans le fichier d'import pour le re-seter.

        $reussite = MySql::create()
            ->setDbName(env('DB_DATABASE'))
            ->setUserName(env('DB_USERNAME'))
            ->setPassword(env('DB_PASSWORD'))            
            ->includeTables($this->tablesToExport)
            //->setHost($host) à rajouter si on est pas en 127.0.0.1
            ->dumpToFile($memosPath.DIRECTORY_SEPARATOR.$filename);
        $puOuPas = $reussite ? 'a pu' : "n'a pas pu" ;
        Log::debug('Le fichier '.$memosPath.DIRECTORY_SEPARATOR.$filename.' '.$puOuPas.' être généré') ;
        
        Log::debug("FIN de l'export");
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ") ;
    }
}
