<?php

namespace App\Console\Commands;

use App\Services\BddService;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Spatie\DbDumper\Databases\MySql;
use Illuminate\Support\Facades\Schema;

class SqlImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bdd:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Importe la base de données la + récente présente à la racine du dossier de templates mémos";


    protected $memosPath = 'resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'memos';

    /* -------------------------------------------------------------------------------*/



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
        Log::debug("Début de l'import de la BDD");

        $folderContent = scandir($this->memosPath);

        //on ne récupère que les fichiers json
        $purgedFolderContent = array_filter($folderContent, function ($entry) /*use ($this->memoPath)*/ {
            if ($entry == "." || $entry == ".." || $entry === ".git" || is_dir($this->memosPath . DIRECTORY_SEPARATOR . $entry)) {
                return false;
            }

            $ext = pathinfo($entry, PATHINFO_EXTENSION);
            return $ext === "sql";
        });

        if (empty($purgedFolderContent)) {
            Log::debug("Aucun fichier JSON trouvé, abandon du processus");
        } else {
            $lastFile = $this->memosPath . DIRECTORY_SEPARATOR . end($purgedFolderContent);
            Log::debug($lastFile);

            //suppression des tables de Bdd::TABLES_TO_EXPORT
            foreach (BddService::TABLES_TO_EXPORT as $key => $tableName) {
                Schema::dropIfExists($tableName);
            }

            // réimport du fichier
            $db = [
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
                'host' => env('DB_HOST'),
                'database' => env('DB_DATABASE')
            ];
            exec("mysql --user={$db['username']} --password={$db['password']} --host={$db['host']} --database {$db['database']} < $lastFile") ;
    
            Log::info("Tentative d'import de ".$lastFile);
        }

        Log::debug("FIN de l'import");
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
    }
}
