<?php

namespace App\Console\Commands;

use App\Models\Domaine;
use Illuminate\Console\Command;
use App\Services\ScandirService;
use Illuminate\Support\Facades\Log;

class DomainesImportJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domaines:import_from_json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import json -> BDD des domaines et sous-domaines';

    protected $memosPath = 'resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'memos' . DIRECTORY_SEPARATOR;

    /* ------------------------------------------------------------------------ */


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
        $msgIntro = $this->description . " (fichier-s présent-s  dans le dossier " . $this->memosPath . ")";
        Log::debug($msgIntro);
        $this->info($msgIntro);

        $jsonFilesInFolder = ScandirService::getFilesOfTypeInFolder("json", $this->memosPath);

        if (empty($jsonFilesInFolder)) {
            $msg = "Aucun fichier JSON trouvé, abandon du processus";
            Log::info($msg);
            $this->info($msg);
            $result = 0 ;
        } else {
            $this->info("Fichiers json trouvés, début d'import...");

            foreach($jsonFilesInFolder as $filename) {
                $this->importJsonFileInDomainesTable($filename) ;
            }


            $result = 1 ;
        }

        $this->info("FIN de la commande");
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");

        return $result ;
    }

    protected function importJsonFileInDomainesTable($filename) {
        $filePath = $this->memosPath . $filename ;
        Log::info($filePath);
        $fileContent = json_decode(file_get_contents($filePath), true) ;

        foreach($fileContent as $slug => $domaineDetails) {
            $newParentDomaine = Domaine::create([
                'id_domaine_parent' => null,
                'slug'              => $slug,
                'label'             => $domaineDetails['label'],
                'type'              => $domaineDetails['type'],
                'comments'          => $domaineDetails['comments'],
                'numero'            => $domaineDetails['numero'],
                'is_public'         => $domaineDetails['is_public'],
            ]);

            $nbInsertedSubDomaines = 0 ;

            foreach($domaineDetails['content'] as $slugSubDomaine => $subDomaineDetails) {
                Domaine::create([
                    'id_domaine_parent' => $newParentDomaine->id,
                    'slug'              => $slugSubDomaine,
                    'label'             => $subDomaineDetails['label'],
                    'type'              => $newParentDomaine->type,
                    'comments'          => $subDomaineDetails['comments'],
                    'numero'            => $subDomaineDetails['numero'],
                    'is_public'         => $subDomaineDetails['is_public'],
                ]);
                $nbInsertedSubDomaines += 1 ;
            }
        }

        $this->info("Nouveau domaine '".$newParentDomaine->label."' inséré (".$nbInsertedSubDomaines. " sous-domaines)");
    }
}
