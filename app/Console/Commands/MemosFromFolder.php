<?php

namespace App\Console\Commands;

use App\Models\Memo;
use App\Models\Memocateg;
use Illuminate\Console\Command;
use App\Services\ScandirService;
use Illuminate\Support\Facades\Log;

class MemosFromFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'memos:folder-to-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scanner le dossier de mémos pour les importer en base de données';

    protected $slugMemocategsByDomains = [];
    protected $slugMemosByDomains = [];

    protected $memosParentPath = 'resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR;

    /* ----------------------------------------------------------------------------------------------------------- */

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //préparation des listes de ce qui existe déjà
        $slugsCategsByDomains = [];
        $categs = Memocateg::select('skey', 'id', 'domain')->get()->groupBy("domain")->toArray();
        foreach ($categs as $aDomain => $listCategs) {
            $slugsCategsByDomains[$aDomain] = collect($listCategs)->pluck("id", "skey")->toArray();
        }
        $this->slugMemocategsByDomains = $slugsCategsByDomains;

        $slugMemosByDomains = [];
        $memos = Memo::select('skey', 'domain')->get()->groupBy("domain")->toArray();

        foreach ($memos as $aDomain => $listMemos) {
            $slugMemosByDomains[$aDomain] = array_flip(collect($listMemos)->pluck("skey")->toArray());
        }
        $this->slugMemosByDomains = $slugMemosByDomains;
        // --


        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
        $msgIntro = $this->description . " (fichier-s présent-s  dans le dossier " . $this->memosParentPath . "/memos )";
        Log::debug($msgIntro);
        $this->info($msgIntro);

        $this->scanAndInsert('memos', $this->memosParentPath, 0);

        $this->info("FIN de la commande");
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
        return 0;
    }

    protected function scanAndInsert($folder, $path, $deep)
    {
        $deep += 1;
        $folderPath = $path . $folder;
        $folderContent = scandir($folderPath);

        foreach ($folderContent as $anEntry) {
            //on passe les emplacements et dossiers git
            if (
                $anEntry == "."
                || $anEntry == ".."
                || $anEntry === ".git"
            ) {
                continue;
            }

            //si dossier : stratégie récursive pour aller scanner dedans
            if (is_dir($folderPath . DIRECTORY_SEPARATOR . $anEntry)) {
                $this->scanAndInsert($anEntry, $folderPath . DIRECTORY_SEPARATOR, $deep);
            }

            //si fichier blade : on checke l'existance de ce fichier et sinon on l'insère
            if (preg_match('/.blade.php$/i', $anEntry)) {
                $base = strtolower(str_replace(".blade.php", "", $anEntry));
                if (preg_match('/^[0-9]{2}_/', $anEntry, $matches)) {
                    $skey = str_replace($matches[0], "", $base);
                    $numero = (int) str_replace("_", "", $matches[0]);
                } else {
                    $skey = $base;
                    $numero = 999;
                }

                //on est dans un dossier de sous-categ
                $arbo = array_reverse(explode(DIRECTORY_SEPARATOR, pathinfo($folderPath . DIRECTORY_SEPARATOR . $anEntry)['dirname']));

                $domains = $arbo[1];
                $subDArray = explode("_", $domains);
                $subdomain = strtolower(end($subDArray));

                $categs = $arbo[0];
                $slugSubC = explode("_", $categs);
                $slugSubcateg = strtolower(end($slugSubC));

                $slug = $subdomain . "_" . $slugSubcateg . "_" . $skey;
                if (isset($this->slugMemosByDomains[$subdomain]) && isset($this->slugMemosByDomains[$subdomain][$slug])) {
                    continue;
                }

                //si le sous-domaine et la categ existent bien, on crée l'entrée pour le fichier
                if (
                    isset($this->slugMemocategsByDomains[$subdomain])
                    && isset($this->slugMemocategsByDomains[$subdomain][$slugSubcateg])
                ) {
                    $nMemo = new Memo();

                    $nMemo->id_user = 1;
                    $nMemo->title = str_replace("_", " ", ucfirst(strtolower($skey)));
                    $nMemo->slug = $slug;
                    $nMemo->skey = $skey;
                    $nMemo->domain = $subdomain;
                    $nMemo->id_memocategs = $this->slugMemocategsByDomains[$subdomain][$slugSubcateg];
                    $nMemo->path = $base;
                    $nMemo->numero = $numero;
                    $nMemo->is_public = 0;

                    //dd($anEntry, $folderPath, $deep, $subdomain, $slugSubcateg, $nMemo);
                    $nMemo->save();
                } else {
                    $msg = "Il manque " ;
                    if(!isset($this->slugMemocategsByDomains[$subdomain])) {
                        $msg.= "le domaine $subdomain et ";
                    }
                    if(!isset($this->slugMemocategsByDomains[$subdomain][$slugSubcateg])) {
                        dd($subdomain, $slugSubcateg, $this->slugMemocategsByDomains);
                        $msg.= "la catégorie $slugSubcateg";
                    }
                    Log::warning($msg);
                    $this->info($msg);
                }
            }

            //si c'est un autre fichier : sera passé sans rien faire
        }
    }
}
