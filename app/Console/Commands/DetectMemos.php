<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DetectMemos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categs:detect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Scanner le dossier de templates des mémos pour ajouter ceux qui ne sont pas encore référencés en base de données (en mode réservé aux admins), et + tard effacer ceux qui n'existent plus en gérannt les références associées";


    /* -------------------------------------------------------------------------------*/

    protected $directoryMemosPath ;
    protected $detectedFiles = [] ;
    

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->directoryMemosPath = /*base_path().DIRECTORY_SEPARATOR.*/"resources".DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."memos" ;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ") ;
        Log::debug("Début de l'analyse du dossier de mémos") ;

        /* 
        scan du dossier de templates mémos pour peupler $detectedFiles avec les fichiers trouvés.
        Leurs stats doivent permettre une comparaison avec la DB et une éventuelle insertion,
        idéalement des catégories comme des mémos
        */
        $this->recursivelyGetFilesInArborescence($this->directoryMemosPath) ;
        Log::debug("Bilan des fichiers mémos trouvés :");
        Log::debug($this->detectedFiles);
        
        Log::debug("FIN de l'analyse du dossier de mémos");
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ") ;
    }


    protected function recursivelyGetFilesInArborescence(?string $absolutepath) {

        $folderContent = $this->getPurgedFolderContent(scandir($absolutepath)) ;
        /*Log::debug("Contenu du dossier à analyser récursivement :") ;
        Log::debug($folderContent) ;*/

        foreach($folderContent as $key => $elementInFolder) {
            $pathOfElement = $absolutepath.DIRECTORY_SEPARATOR.$elementInFolder ;

            if(is_dir($pathOfElement)) {
                //Log::debug($pathOfElement . " est un dossier, on analysera son contenu récursivement");
                $this->recursivelyGetFilesInArborescence($pathOfElement) ;
            } else {
                $nowInSQL = Carbon::now()->format('Y-m-d H')  ;
                
                //nom du fichier sans extension et sluggifié
                $filename = Str::slug(trim(str_replace(".blade.php", "", $elementInFolder))) ;

                //chemin relatif depuis la racine des dossiers de template memos (sans l'extension de fichier), ex inf_prog/git/00_intro
                $filepath = str_replace($this->directoryMemosPath, "", $pathOfElement) ;
                Log::debug("Fichier : ". $filename. " dans ".$filepath) ;

                $memos[] = [
                    "id_user" => 1,
                    "id_memocategs" => "",
                    "title" => $filename,
                    "slug"=> $filename,
                    "path"=> $filepath,
                    "numero"=> "",
                    "is_public"=> 0,
                    "created_at" => $nowInSQL,
                    "updated_at" => $nowInSQL
                ] ;
                //Log::debug($pathOfElement . " est un fichier");
            }
        }
    }

    protected function getPurgedFolderContent(?array $folder) {
        $purgedFolderContent = array_filter($folder, function($entry) {
            return $entry !== "." && $entry !== ".." && $entry !== ".git" ;
        }) ;
        return $purgedFolderContent ;
    }
}
