<?php

namespace App\Console\Commands;

use App\Models\Domaine;
use App\Models\Memo;
use App\Models\Memocateg;
use Illuminate\Console\Command;
use App\Services\ScandirService;
use Illuminate\Support\Facades\Log;

class CategsFromFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categs:folder-to-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scanner le dossier de mémos pour importer en base de données un début de catégories (à retoucher manuellement ensuite)';

    protected $subdomainsIdsBySlug = [];
    private $categsIdsBySlug = [] ;

    protected $memosParentPath = 'resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR;

    /* ----------------------------------------------------------------------------------------------------------- */

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->subdomainsIdsBySlug = Domaine::select(['id', 'slug'])->where([['id_domaine_parent', '!=', null]])->get()->keyBy('slug')->toArray() ;
        $this->categsIdsBySlug = Memocateg::select(['id', 'slug'])->get()->keyBy('slug')->toArray() ;

        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
        $msgIntro = $this->description . " (fichier-s présent-s  dans le dossier " . $this->memosParentPath . "/memos )";
        Log::debug($msgIntro);
        $this->info($msgIntro);

        $this->scanAndInsert('memos', $this->memosParentPath);

        $this->info("FIN de la commande");
        Log::debug("8<- - - - - - - - - - - - - - - - - - - - - - - - - - - - ");
        return 0;
    }

    protected function scanAndInsert($folder, $path)
    {
        $folderPath = $path . $folder;
        $folderContent = scandir($folderPath);
        $nbImportedCategs = 0 ;
        $nbImportedSubcategs = 0 ;

        foreach ($folderContent as $aSubdomain) {
            //on passe les emplacements et dossiers git, ainsi que les fichiers : seuls les dossiers nous intéressent
            if (
                $aSubdomain == "."
                || $aSubdomain == ".."
                || $aSubdomain === ".git"
                || !is_dir($folderPath . DIRECTORY_SEPARATOR . $aSubdomain)
            ) {
                continue;
            }

            // on récup la fin du nom de dossier pour avoir le slug de la subcateg concernée, donc son id
            $domainsImplicated = explode("_", $aSubdomain) ;
            $slugSubdomain = end($domainsImplicated) ;

            //on n'intègrera les catégories que si leur sous-domaine existe bien
            if(!isset($this->subdomainsIdsBySlug[$slugSubdomain])) {
                $msg = "WARNING : le domaine $slugSubdomain ne semble pas exister en base de données" ;
                Log::warning($msg);
                $this->info($msg);
                continue ;
            }

            $subdomainContent = scandir($folderPath . DIRECTORY_SEPARATOR . $aSubdomain);

            foreach ($subdomainContent as $aSubcateg) {
                if (
                    $aSubcateg == "."
                    ||  $aSubcateg == ".."
                    ||  $aSubcateg === ".git"
                    || !is_dir($folderPath . DIRECTORY_SEPARATOR . $aSubdomain . DIRECTORY_SEPARATOR . $aSubcateg)
                ) {
                    continue;
                }
                $categsImplicated = explode('_', $aSubcateg) ;
                $slugCateg = $categsImplicated[0] ;
                $slugSubCateg = $categsImplicated[1] ;

                // si la catégorie parente n'existe pas encore, on l'importe
                if(!isset($this->categsIdsBySlug[$slugCateg])) {
                    $idInsertedCateg = $this->createCateg($slugCateg, $slugSubdomain, $aSubdomain, $aSubcateg, $parentCategId = null) ;
                    $this->categsIdsBySlug[$slugCateg] = $idInsertedCateg ;
                    $nbImportedCategs++ ;
                }

                //on ajoute la catégorie fille
                $idInsertedSubcateg = $this->createCateg($slugSubCateg, $slugSubdomain, $aSubdomain, $aSubcateg, $idInsertedCateg) ;
                $this->categsIdsBySlug[$slugSubCateg] = $idInsertedSubcateg ;
                $nbImportedSubcategs++ ;
            }

            //bilan des imports
            $msg = "BILAN : $nbImportedCategs catégories importées et $nbImportedSubcategs sous-catégories importées" ;
            Log::warning($msg);
            $this->info($msg);
        }
    }

    private function createCateg($skey, $domain, $aSubdomain, $aSubcateg, $parentCategId = null) {
        //dd($skey, $domain, $aSubdomain, $aSubcateg, $parentCategId);
        $nCateg = new Memocateg();

        $nCateg->id_user = 1;
        $nCateg->id_memocategs_parent = $parentCategId ;
        $nCateg->label = ucfirst($skey) ;
        $nCateg->slug = $parentCategId !== null ? $aSubdomain. "_". Memocateg::findOrFail($parentCategId)->skey ."_".$skey  : $aSubdomain. "_". $skey ; //subdomainskey_parentskey_skey
        $nCateg->skey = $skey ;
        $nCateg->domain = $domain ;
        //TODO path uniquement si subcateg
        $nCateg->path = $aSubdomain . DIRECTORY_SEPARATOR . $aSubcateg /*. DIRECTORY_SEPARATOR . $slug */;
        $nCateg->numero = 99 ;
        $nCateg->is_public = 1;

        $nCateg->save();

        return $nCateg->id ;
    }
}
