<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\VocabulaireImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportVocab extends Command
{
    protected $vocabPath = 'resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'memos' . DIRECTORY_SEPARATOR;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vocabulaire:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importer les lignes de vocabulaire qui sont dans le tableur';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $vocabFilePath = $this->vocabPath . "vocabulaire.ods" ;
        Excel::import(new VocabulaireImport, $vocabFilePath) ;
        
        return 0;
    }
}
