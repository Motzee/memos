<?php

namespace App\Providers;

use App\Models\Memocateg;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //WARNING : TOUT passe par là, même les commandes artisan, donc il faut impérativement traiter le cas où on n'a pas encore déployé la DB
        if(!App::runningUnitTests() && !App::runningInConsole()) {
            if (is_null(Auth::user())) {
                $parentCategs = Memocateg::select(['id', 'id_user', 'label', 'slug', 'is_public'])
                    ->where([
                        ['is_public', 1],
                        ['id_memocategs_parent', null]
                    ])->orderBy('numero')
                    ->get()->toArray();
            }
            else {
                $parentCategs = Memocateg::select(['id', 'id_user', 'label', 'slug', 'is_public'])
                    ->where([
                        ['is_public', 1],
                        ['id_memocategs_parent', null]
                    ])->orWhere([
                        ['id_user', Auth::user()->id],
                        ['id_memocategs_parent', null]
                    ])->orderBy('numero')
                    ->get()->toArray();
            }
            foreach ($parentCategs as $key => $aParentCateg) {
                $parentCategs[$key]['label_short'] = explode(" ", $aParentCateg['label'])[0] ;
            }
            view()->share('nav_content', $parentCategs);
        }
    }
}
