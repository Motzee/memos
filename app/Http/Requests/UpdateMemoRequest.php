<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMemoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $memo = $this->route()->parameter('memo');

        return (Auth::user()->id == $memo->id_user);

        //autre système : return $this->user()->can('view settings'); ou bien un système de gate (porte)
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_memocategs' => 'required',
            'skey'  => 'required|min:3|max:255',
            'title' => 'required|min:3|max:255',
            'path' => 'required|min:3|max:255',
            'domain'  => 'required|min:2|max:255',
            'numero'    => '',
            'is_public' => ''
        ];
    }

    /**
     * Customize error messages
     *
     * @return array
     */
    public function messages() {
        return [
            'title.required'            => 'Ce champ est requis',
            'title.min'                 => 'Ce champ ne peut faire moins de 3 caractères', 
            'title.max'                 => 'Ce champ ne peut excéder 255 caractères',

            'id_memocategs.required'    => 'Ce champ est requis',

            'path.required'            => 'Ce champ est requis',
            'path.min'                 => 'Ce champ ne peut faire moins de 3 caractères', 
            'path.max'                 => 'Ce champ ne peut excéder 255 caractères',
        ];
    }
}
