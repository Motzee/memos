<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVocabwordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_equivalent_fr'  => 'nullable|integer',
            'lang'              => 'required|min:2|max:255',
            'word'              => 'required|min:1|max:255',
            'gender'            => 'nullable|min:3|max:255',
            'class'             => 'nullable|min:3|max:255',
            'picture'           => 'nullable|min:3|max:255',
            'details'           => 'nullable',
            'level'             => 'nullable|integer',
            'is_public'         => 'nullable|integer',
        ];
    }

        /**
     * Customize error messages
     *
     * @return array
     */
    public function messages() {
        return [
        ];
    }
}
