<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMemocategsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_memocategs_parent' => '',
            'skey'  => 'required|min:3|max:255',
            'label' => 'required|min:3|max:255',
            'slug' => 'required|min:3|max:255',
            'domain' => 'required|min:2|max:255',
            'path' => 'required|min:3|max:255',
            'numero' => '',
            'is_public' => ''
        ];
    }

    /**
     * Customize error messages
     *
     * @return array
     */
    public function messages() {
        return [
            'label.required'           => 'Ce champ est requis',
            'label.min'                => 'Ce champ ne peut faire moins de 3 caractères', 
            'label.max'                => 'Ce champ ne peut excéder 255 caractères',

            'slug.required'            => 'Ce champ est requis',
            'slug.min'                 => 'Ce champ ne peut faire moins de 3 caractères', 
            'slug.max'                 => 'Ce champ ne peut excéder 255 caractères',

            'domain.required'            => 'Ce champ est requis',
            'domain.min'                 => 'Ce champ ne peut faire moins de 2 caractères', 
            'domain.max'                 => 'Ce champ ne peut excéder 255 caractères',

            'path.required'            => 'Ce champ est requis',
            'path.min'                 => 'Ce champ ne peut faire moins de 3 caractères', 
            'path.max'                 => 'Ce champ ne peut excéder 255 caractères',
        ];
    }
}
