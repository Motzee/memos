<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMemoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Indicates if the validator should stop on the first rule failure.
     *
     * @var bool
     */    protected $stopOnFirstFailure = true;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_memocategs' => 'required',
            'title' => 'required|min:3|max:255',
            'skey'  => 'required|min:3|max:255',
            'domain'  => 'required|min:2|max:255',
            'path' => 'required|min:3|max:255',
            'numero'    => '',
            'is_public' => ''
        ];
    }

    /**
     * Customize error messages
     *
     * @return array
     */
    public function messages() {
        return [
            'title.required'           => 'Ce champ est requis',
            'title.min'                => 'Ce champ ne peut faire moins de 3 caractères', 
            'title.max'                => 'Ce champ ne peut excéder 255 caractères',

            'skey.required'            => 'Ce champ est requis',
            'skey.min'                 => 'Ce champ ne peut faire moins de 3 caractères', 
            'skey.max'                 => 'Ce champ ne peut excéder 255 caractères',

            'domain.required'            => 'Ce champ est requis',
            'domain.min'                 => 'Ce champ ne peut faire moins de 2 caractères', 
            'domain.max'                 => 'Ce champ ne peut excéder 255 caractères',

            'id_memocategs.required'   => 'Ce champ est requis',

            'path.required'            => 'Ce champ est requis',
            'path.min'                 => 'Ce champ ne peut faire moins de 3 caractères', 
            'path.max'                 => 'Ce champ ne peut excéder 255 caractères',
        ];
    }
}
