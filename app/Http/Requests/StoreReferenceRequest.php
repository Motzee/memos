<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreReferenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_memocateg'  => '',
            'id_memo'       => '',
            'label'         => 'required|min:3|max:255',
            'url'           => 'max:255',
            'is_rss'        => '',
            'is_veille'     => '',
            'is_public'     => ''
        ];
    }

    /**
     * Customize error messages
     *
     * @return array
     */
    public function messages() {
        return [
            'label.required'           => 'Ce champ est requis',
            'label.min'                => 'Ce champ ne peut faire moins de 3 caractères', 
            'label.max'                => 'Ce champ ne peut excéder 255 caractères'
        ];
    }
}
