<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Domaine;
use App\Models\Memocateg;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct() {
        
    }

    public function home() {
        if(is_null(Auth::user())) {
            $constraints = [['id_domaine_parent', "=", null], ['is_public', "=", 1]] ;
        } else {
            $constraints = [['id_domaine_parent', "=", null]] ; //TODO restreindre aux publics et possédés
        }
        $domains = Domaine::where($constraints)->get()->sortBy('numero') ;
        
        if(is_null(Auth::user())) {
            $subConstraints = [['id_domaine_parent', "<>", null], ['is_public', "=", 1]] ;
        } else {
            $subConstraints = [['id_domaine_parent', "<>", null]] ; //TODO restreindre aux publics et possédés
        }
        $subDomains = Domaine::where($subConstraints)->get()->sortBy('numero')->groupBy('id_domaine_parent') ;

        $menuContent = [] ;

        return view('home', compact('domains', 'subDomains', 'menuContent'));
    }

    public function subDomaineContent(string $slugSubDomaine) {
        if(is_null(Auth::user())) {
            $constraints = [
                ['slug', "=", $slugSubDomaine],
                ['is_public', "=", 1]
            ] ;
        } else {
            $constraints = [
                ['slug', "=", $slugSubDomaine]
            ] ; //TODO restreindre aux publics et possédés
        }
        $subDomain = Domaine::where($constraints)->first();

        //404 si pas trouvé ou pas les droits
        if(empty($subDomain)) {
            throw new Exception("Le sous-domaine sélectionné n'a pas été trouvé") ;
        }

        if(is_null(Auth::user())) {
            $menuConstraints = [
                ['domain', "=", $slugSubDomaine],
                ['id_memocategs_parent', "=", null],
                ['is_public', "=", 1]
            ] ;
        } else {
            $menuConstraints = [
                ['domain', "=", $slugSubDomaine],
                ['id_memocategs_parent', "=", null]
            ] ; //TODO restreindre aux publics et possédés
        }
        $menuContent = Memocateg::where($menuConstraints)->get()->keyBy('slug')->sortBy('numero') ;

        return view('subdomain', compact('subDomain', 'menuContent'));
    }
}
