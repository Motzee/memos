<?php

namespace App\Http\Controllers;

use App\Models\Reference;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreReferenceRequest;
use App\Http\Requests\UpdateReferenceRequest;

class ReferencesController extends Controller
{
    protected $boolean_fields = [
        'is_rss',
        'is_veille',
        'is_public'
    ] ;
    
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->middleware('auth')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null(Auth::user())) {
            $references = Reference::where('is_public', 1)->get();
        }
        else {
            $references = Reference::all();
        }

        $menuContent = [] ;

        return view('resources.references.index', compact('references', 'menuContent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ref = new Reference;
        if(array_key_exists('id_memo', $_GET)) {
            $ref->id_memo = (int) $_GET['id_memo'] ;
        }
        if(array_key_exists('id_memocateg', $_GET)) {
            $ref->id_memocateg = (int) $_GET['id_memocateg'] ;
        }

        $menuContent = [] ;

        return view('resources.references.create', compact(['ref', 'menuContent'])) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReferenceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReferenceRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['id_user'] = Auth::id();
        $validatedData = $this->completeBooleanFields($validatedData) ;
                
        $nRef = Reference::create($validatedData);

        return redirect()->route('references.index')->with('success', 'La référence a bien été ajoutée');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reference  $reference
     * @return \Illuminate\Http\Response
     */
    public function edit(Reference $reference)
    {
        $this->rejectIfUserNotOwnerOf($reference) ;

        $ref = $reference ;

        $menuContent = [] ;

        return view('resources.references.edit', compact(['ref', 'menuContent'])) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateReferenceRequest  $request
     * @param  \App\Models\Reference  $reference
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReferenceRequest $request, Reference $reference)
    {
        $this->rejectIfUserNotOwnerOf($reference) ;

        $validatedData = $request->validated();
        $validatedData = $this->completeBooleanFields($validatedData) ;

        $reference->update($validatedData);

        return redirect()->route('references.index')->with('success', 'La référence a bien été modifiée');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reference  $reference
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reference $reference)
    {
        $this->rejectIfUserNotOwnerOf($reference) ;

        $reference->delete() ;

        return redirect()->route('references.index')->with('success', 'La référence a bien été supprimée');
    }

    /* *********************************************************************** */

    protected function rejectIfUserNotOwnerOf($object)
    {
        if (Auth::user()->id !== $object->id_user) {
            abort(401);
        }
    }

    protected function completeBooleanFields(array $validatedData) {
        foreach($this->boolean_fields as $aField) {
            if(!array_key_exists($aField, $validatedData)) {
                $validatedData[$aField] = 0 ;
            }
        }
        return $validatedData ;
    }
}
