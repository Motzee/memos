<?php

namespace App\Http\Controllers;

use App\Enums\LanguageWordEnum;
use App\Models\Vocabword;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreVocabwordRequest;
use App\Http\Requests\UpdateVocabwordRequest;

class VocabwordController extends Controller
{

    protected $boolean_fields = [
        'is_public'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $words = Vocabword::where([['lang', '=', LanguageWordEnum::FR]])->paginate(15)/*->get()*/ ;

        return view('resources.vocabwords.index', compact('words'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function devine()
    {
        $themes = Vocabword::has('wordsOfThisTheme')->get()->toArray();

        /*
        $idWordsWithPictures = Vocabword::select('id')->where([['lang', '=', LanguageWordEnum::FR], ['picture', '<>', null]])->inRandomOrder()/*->limit(20)*//*->get();
     /*   $allWords = [] ;
        $animals = Vocabword::where([['word', "=", "animal"]])->first();
        dd($idWordsWithPictures->toArray());
        dd($animals->wordsOfThisTheme->toArray());*/

        return view('resources.vocabwords.devine', compact('themes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $word = new Vocabword;
        return view('resources.vocabwords.create', compact(['word']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreVocabwordRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVocabwordRequest $request)
    {
        $validatedData = $request->validated();
        //$validatedData['id_user'] = Auth::id();
        $validatedData = $this->completeBooleanFields($validatedData);

        $nEntry = Vocabword::create($validatedData);

        return redirect()->route('vocabwords.index')->with('success', 'Le mot a bien été ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vocabword  $vocabword
     * @return \Illuminate\Http\Response
     */
    public function show(Vocabword $vocabword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vocabword  $vocabword
     * @return \Illuminate\Http\Response
     */
    public function edit(Vocabword $vocabword)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateVocabwordRequest  $request
     * @param  \App\Models\Vocabword  $vocabword
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVocabwordRequest $request, Vocabword $vocabword)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vocabword  $vocabword
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vocabword $vocabword)
    {
        //
    }

    protected function completeBooleanFields(array $validatedData)
    {
        foreach ($this->boolean_fields as $aField) {
            if (!array_key_exists($aField, $validatedData)) {
                $validatedData[$aField] = 0;
            }
        }
        return $validatedData;
    }
}
