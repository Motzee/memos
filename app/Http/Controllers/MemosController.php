<?php

namespace App\Http\Controllers;

use App\Models\Memo;
use App\Models\Memocateg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreMemoRequest;
use App\Http\Requests\UpdateMemoRequest;
use App\Models\Domaine;

class MemosController extends Controller
{
    protected $boolean_fields = [
        'is_public'
    ];

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // $this->middleware('auth')->except(['index', 'show']);
        //TODO
        //$this->middleware('byowner')->only(['edit', 'update', 'destroy]);
    }

    /* *********************************************************************** */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Facades\View;
     */
    public function index()
    {
        if (is_null(Auth::user())) {
            $memos = Memo::select(['id', 'title', 'id_memocategs'])
                ->where('is_public', 1)
                ->orderBy('numero')
                ->get();

            $categs = Memocateg::where('is_public', 1)
                ->orderBy('numero')
                ->get()
                ->keyBy('id')
                ->toArray();
        } else {
            $memos = Memo::where('id_user', Auth::user()->id)
                ->orWhere('is_public', 1)
                ->orderBy('numero')
                ->get();

            $categs = Memocateg::where('id_user', Auth::user()->id)
                ->orderBy('numero')
                ->get()
                ->keyBy('id')
                ->toArray();
        }

        foreach ($memos as $key => $aMemo) {
            //dd($categs[$aMemo->id_memocategs]);
            if (!array_key_exists('memos', $categs[$aMemo->id_memocategs])) {
                $categs[$aMemo->id_memocategs]['memos'] = [];
            }
            $categs[$aMemo->id_memocategs]['memos'][] = $aMemo;
        }
        $menuContent = Memocateg::where([
            ['domain', "=", $aMemo->domain],
            ['id_memocategs_parent', "=", null],
            ['is_public', "=", 1]
        ])->get()->keyBy('slug')->sortBy('numero');
        
        return view('resources.memos.index', compact('categs', 'menuContent'));
    }

    /* *********************************************************************** */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Support\Facades\View;
     */
    public function create()
    {
        //$categs = $this->findMemocategs();
        $domains = $this->listDomains();
        $categs = $this->listCategs();

        $memo = new Memo;
        return view('resources.memos.create', compact(['memo', 'categs', 'domains']));
    }

    /* *********************************************************************** */

    /**
     * Store a newly created resource in storage.
     * @param  \App\Http\Requests\StoreMemoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMemoRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['id_user'] = Auth::id();
        $subcateg = Memocateg::findOrFail($validatedData['id_memocategs']) ;
        $validatedData['slug'] = $validatedData['domain'] . "_" .  $subcateg->slug . "_" . $validatedData['skey'] ;
        $validatedData = $this->completeBooleanFields($validatedData);
        $nMemo = Memo::create($validatedData);

        return redirect()->route('memos.index')->with('success', 'Le mémo a bien été ajouté');
    }

    /* *********************************************************************** */

    /**
     * Display the specified resource.

     * @return \Illuminate\Support\Facades\View;
     */
    public function show(string $subDomain, string $subCateg, string $memo)
    {
        $memo = Memo::select('memos.*', 'memocategs.label')->join("memocategs", "memocategs.id", "=", "memos.id_memocategs")
            ->where([["memos.skey", "=", $memo], ['memocategs.slug', "=", $subCateg]])->first();

        if (!is_null($memo) && ($memo->is_public === 1 || (!!Auth::user() && $memo->id_user === Auth::user()->id))) {
            $pCateg = Memocateg::findOrFail($memo->id_memocategs);
            $categs = [
                [
                    'label' => $pCateg->label,
                    'slug'  => $pCateg->slug
                ]
            ];
            if (!is_null($pCateg->id_memocategs_parent)) {
                $pCateg = Memocateg::findOrFail($pCateg->id_memocategs_parent);
                $categs[] = [
                    'label' => $pCateg->label,
                    'slug'  => $pCateg->slug
                ];
            }
            $parentCategs = array_reverse($categs);

            $menuContent = Memocateg::where([
                ['domain', "=", $memo->domain],
                ['id_memocategs_parent', "=", null],
                ['is_public', "=", 1]
            ])->get()->keyBy('slug')->sortBy('numero');
            //dd($memo->getPath());
            return view('resources.memos.show', compact(['memo', 'parentCategs', 'menuContent']));
        } else {
            dd("Woops");
            abort(404);
        }
    }

    /* *********************************************************************** */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Memo  $memo
     * @return \Illuminate\Support\Facades\View;
     */
    public function edit(Memo $memo)
    {
        $this->rejectIfUserNotOwnerOf($memo);

        //$categs = $this->findMemocategs();
        $categs = $this->listCategs();
        $domains = $this->listDomains();

        $menuContent = Memocateg::where([
            ['domain', "=", $memo->domain],
            ['id_memocategs_parent', "=", null],
            ['is_public', "=", 1]
        ])->get()->keyBy('slug')->sortBy('numero');
        
        return view('resources.memos.edit', compact(['memo', 'categs', 'domains', 'menuContent']));
    }

    /* *********************************************************************** */

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMemoRequest  $request
     * @param  \App\Models\Memo  $memo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMemoRequest $request, Memo $memo)
    {
        $this->rejectIfUserNotOwnerOf($memo);

        $validatedData = $request->validated();
        $validatedData = $this->completeBooleanFields($validatedData);

        $memo->update($validatedData);

        return redirect()->route('memos.show2', [
            'subDomain' => $memo->domain,
            'subCateg' => $memo->memocateg->slug,
            'memo' => $memo->skey
        ])->with('success', 'Le mémo a bien été modifié');
    }

    /* *********************************************************************** */

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Memo  $memo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Memo $memo)
    {
        $this->rejectIfUserNotOwnerOf($memo);

        $memo->delete();

        return redirect()->route('memos.index')->with('success', 'Le mémo a bien été supprimé');
    }

    /* *********************************************************************** */

    protected function rejectIfUserNotOwnerOf($object)
    {
        if (Auth::user()->id !== $object->id_user) {
            abort(401);
        }
    }

    //TODO  only public or owned by user
    protected function listDomains()
    {
        $domains = Domaine::whereNull('id_domaine_parent')->get();

        $domainsAndSub = [];

        return $domains;
    }
    protected function listCategs()
    {
        $allCategs = Memocateg::whereNull('id_memocategs_parent')->get();
        $categsByDomainSlug = [];



        return $allCategs;
    }

    protected function completeBooleanFields(array $validatedData)
    {
        foreach ($this->boolean_fields as $aField) {
            if (!array_key_exists($aField, $validatedData)) {
                $validatedData[$aField] = 0;
            }
        }
        return $validatedData;
    }

    /*protected function findMemocategs() {
        $parentCategs = Memocateg::select(['id', 'label'])
            ->where([
                ['is_public', 1],
                ['id_memocategs_parent', null]
            ])->orWhere([
                ['id_user', Auth::user()->id],
                ['id_memocategs_parent', null]
            ])->orderBy('numero')
            ->get()->keyBy('id');
        //$parentCategsIds = $parentCategs->keys()->toArray() ;
        $categs = $parentCategs->toArray() ;
                 
        //TODO ne prendre que celles dans les id des parents
        $subCategs = Memocateg::select(['id', 'label', 'id_memocategs_parent', 'numero'])
            ->where([
                ['is_public', 1],
                ['id_memocategs_parent', '<>', NULL]
            ])->orWhere([
                ['id_user', Auth::user()->id],
                ['id_memocategs_parent', '<>', NULL]
            ])->orderBy('numero')
            ->get()->toArray();

        foreach ($subCategs as $key => $aSubCateg) {
            if(!array_key_exists('subcategs', $categs[$aSubCateg['id_memocategs_parent']])) {
                $categs[$aSubCateg['id_memocategs_parent']]['subcategs'] = [] ;
            }
            $categs[$aSubCateg['id_memocategs_parent']]['subcategs'][$aSubCateg['numero']] = $aSubCateg ;
        }

        return $categs ;
    }*/
}
