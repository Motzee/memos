<?php

namespace App\Http\Controllers;

use App\Models\Memocateg;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreMemocategsRequest;
use App\Http\Requests\UpdateMemocategsRequest;
use App\Models\Domaine;
use App\Models\Memo;

class MemocategsController extends Controller
{
    protected $boolean_fields = [
        'is_public'
    ];

    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth')->except(['index', 'show']);
    }

    public function categContent(string $slugSubDomaine, string $slugSubCateg)
    {
        //TODO le problème d'ici, c'est qu'on récup les categs publiques, mais que dans leur memos() il y a les privés... du coup les mémos privés se listent, bien qu'inaccessibles
        if (is_null(Auth::user())) {
            $data = Memocateg::with(['childrenCategs' => function ($query) {
                $query->where('is_public', 1);
            }])
            ->where([
                ['is_public', "=", 1],
                ['slug', "=", $slugSubCateg]
            ])
            ->orderBy('numero', 'ASC')
            ->firstOrFail();
        } else {
            $data = Memocateg::with(['childrenCategs' => function ($query) {
                $query->where('is_public', 1)
                    ->orWhere('id_user', Auth::user()->id);
            }])
            ->where([
                ['is_public', "=", 1],
                ['slug', "=", $slugSubCateg]
            ])
            ->orWhere([
                ['id_user', "=", Auth::user()->id],
                ['slug', "=", $slugSubCateg]
            ])
            ->orderBy('numero', 'ASC')
            ->firstOrFail();
        }

        /*
        if (is_null(Auth::user())) {
            $data = Memocateg::where([
                ['is_public', "=",  1],
                ['domain', "=", $slugSubDomaine]
                //['id_memocategs_parent', '<=>', null]
            ])
                ->orderBy('numero', 'ASC')
                ->get();
            dd($data);
        } else {
            $data = Memocateg::where([
                ['is_public', 1],
                ['id_memocategs_parent', '=', null]
            ])
                ->orWhere([
                    ['id_user', Auth::user()->id],
                    ['id_memocategs_parent', '=', null]
                ])
                ->orderBy('numero', 'ASC')
                ->get();
        }*/
        $menuContent = Memocateg::where([
            ['domain', "=", $slugSubDomaine],
            ['id_memocategs_parent', "=", null],
            ['is_public', "=", 1]
        ])->get()->keyBy('slug')->sortBy('numero');
       // dd($menuContent);
        return view('resources.memocategs.index2', compact('menuContent', 'data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (is_null(Auth::user())) {
            /*$data = Memocateg::where([
                ['is_public', 1],
                ['id_memocategs_parent', '<=>', null]
            ])
                ->orderBy('numero', 'ASC')
                ->get();*/
            dd('unauthorized');
        } else {
            $parentDomaines/*ById*/ = Domaine::where([['id_domaine_parent', "=", null]])->orderBy('numero', 'ASC')->get()/*->keyBy('id')->toArray()*/;
            //dd($parentDomaines);
            /*$childrenDomains = Domaine::orderBy('numero', 'ASC')->get()->keyBy('id_domaine_parent')->toArray();
            foreach($childrenDomains as $parentId => $aGroupOfChildren) {
                $parentDomainesById[$parentId]['childrenDomains'] = $aGroupOfChildren ;
            }
                dd($parentDomainesById);
*/
            $data = Memocateg::where([
                ['is_public', 1],
                ['id_memocategs_parent', '=', null]
            ])
                ->orWhere([
                    ['id_user', Auth::user()->id],
                    ['id_memocategs_parent', '=', null]
                ])
                ->orderBy('numero', 'ASC')
                ->get();
        }

        return view('resources.memocategs.index', compact('parentDomaines', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $domaines = Domaine::whereNull('id_domaine_parent')->get();
        $memocateg = new Memocateg;
        $parentCategs = Memocateg::whereNull('id_memocategs_parent')->get();
        return view('resources.memocategs.create', compact(['domaines', 'memocateg', 'parentCategs']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMemocategsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMemocategsRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['id_user'] = Auth::id();
        $validatedData = $this->completeBooleanFields($validatedData);

        $nCateg = Memocateg::create($validatedData);

        return redirect()->route('memocategs.index')->with('success', 'La catégorie a bien été ajoutée');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Memocateg  $memocateg
     * @return \Illuminate\Http\Response
     */
    public function show(Memocateg $memocateg)
    {
        if ($memocateg->is_public !== 1 && (!!Auth::user() && $memocateg->id_user !== Auth::user()->id)) {
            abort(404);
        } else {

            $categ = Memocateg::find($memocateg->id);
            //TODO peut-être besoin d'un menu ?

            $menuContent = Memocateg::where([
                ['domain', "=", $categ->domain],
                ['id_memocategs_parent', "=", null],
                ['is_public', "=", 1]
            ])->get()->keyBy('slug')->sortBy('numero');

            return view('resources.memocategs.show', compact('categ', 'menuContent'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Memocateg  $memocateg
     * @return \Illuminate\Http\Response
     */
    public function edit(Memocateg $memocateg)
    {
        $this->rejectIfUserNotOwnerOf($memocateg);

        $domaines = Domaine::whereNull('id_domaine_parent')->get();

        $parentCategs = Memocateg::whereNull('id_memocategs_parent')->get();
        return view('resources.memocategs.edit', compact(['domaines', 'memocateg', 'parentCategs']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMemocategsRequest  $request
     * @param  \App\Models\Memocateg  $memocateg
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMemocategsRequest $request, Memocateg $memocateg)
    {
        $this->rejectIfUserNotOwnerOf($memocateg);

        $validatedData = $request->validated();
        $validatedData = $this->completeBooleanFields($validatedData);

        $memocateg->update($validatedData);

        return redirect()->route('memocategs.show', $memocateg->slug)->with('success', 'La catégorie a bien été modifiée');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Memocateg  $memocateg
     * @return \Illuminate\Http\Response
     */
    public function destroy(Memocateg $memocateg)
    {
        $this->rejectIfUserNotOwnerOf($memocateg);

        $memocateg->delete();

        return redirect()->route('memocategs.index')->with('success', 'La catégorie a bien été supprimée');
    }

    /* *********************************************************************** */

    protected function rejectIfUserNotOwnerOf($object)
    {
        if (Auth::user()->id !== $object->id_user) {
            abort(401);
        }
    }

    protected function completeBooleanFields(array $validatedData)
    {
        foreach ($this->boolean_fields as $aField) {
            if (!array_key_exists($aField, $validatedData)) {
                $validatedData[$aField] = 0;
            }
        }
        return $validatedData;
    }
}
