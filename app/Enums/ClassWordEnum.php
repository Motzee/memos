<?php

namespace App\Enums ;

abstract class ClassWordEnum {

	const NOM = "nom";
	const ADJECTIF = "adjectif";
	const VERBE = "verbe";
	const ADVERBE = "adverbe";
	const DETERMINANT = "determinant";
	const PRONOM = "pronom";
	const PREPOSITION = "preposition";
	const CONJONCTION_COORDINATION = "conjonction_coordination";
	const CONJONCTION_SUBORDINATION = "conjonction_subordination";
	const INTERJECTION = "interjection";
	
	

	/** @var array user friendly named roles */
	protected static $entries = [
		self::NOM  				        	=> ['libelle' => 'nom', 'type' => 'variable', 'abbr' => 'nom'],
		self::ADJECTIF  		        	=> ['libelle' => 'adjectif', 'type' => 'variable', 'abbr' => 'adj.'],
		self::VERBE  			        	=> ['libelle' => 'verbe', 'type' => 'variable', 'abbr' => 'v.'],
		self::ADVERBE  			        	=> ['libelle' => 'adverbe', 'type' => 'variable', 'abbr' => 'adv.'],
		self::DETERMINANT  		        	=> ['libelle' => 'déterminant', 'type' => 'variable', 'abbr' => 'det.'],
		self::PRONOM  			        	=> ['libelle' => 'pronom', 'type' => 'variable', 'abbr' => 'prn.'],
		self::PREPOSITION		        	=> ['libelle' => 'préposition', 'type' => 'variable', 'abbr' => 'prep.'],
		self::CONJONCTION_COORDINATION  	=> ['libelle' => 'conjonction de coordination', 'type' => 'variable', 'abbr' => 'conjcoo.'],
		self::CONJONCTION_SUBORDINATION  	=> ['libelle' => 'conjonction de subordination', 'type' => 'variable', 'abbr' => 'conjsub.'],
		self::INTERJECTION  	        	=> ['libelle' => 'interjection', 'type' => 'variable', 'abbr' => 'intj.']
	];

	/**
	 * @return array
	 */
	public static function getCodes(): array
	{
		return [
			self::NOM,
		    self::ADJECTIF,
	    	self::VERBE,
    		self::ADVERBE,
	    	self::DETERMINANT,
	    	self::PRONOM,
	    	self::PREPOSITION,
	    	self::CONJONCTION_COORDINATION,
	    	self::CONJONCTION_SUBORDINATION,
	    	self::INTERJECTION
		];
	}

	/**
	 * @return array
	 */
	public static function getLibellesByKeys(): array
	{
		$roles = array();

		foreach (self::all() as $key => $type) {
			$type[$key] = $type['libelle'];
		}
		
		return $roles;
	}

	/**
	 * @param string $code
	 * 
	 * @return int
	 */
	public static function getTypeByCode(string $code): string
	{
		return self::$entries[$code]['type'];
	}

	/**
	 * @param string $code
	 * 
	 * @return int
	 */
	public static function getAbbrByCode(string $code): string
	{
		return self::$entries[$code]['abbr'];
	}

	/**
	 * @return string
	 */
	public static function getLibelleByCode(string $code): string
	{
		return self::$entries[$code]['libelle'];
	}

	/**
	 * @return array
	 */
	public static function all(): array
	{
		return self::$entries;
	}
}