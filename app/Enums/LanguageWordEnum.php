<?php

namespace App\Enums ;

abstract class LanguageWordEnum {

	const FR = "fr";
	const EN = "en";
    const DE = "de";
    const BZH = "bzh";
    const EO = "eo";
	

	/** @var array user friendly named roles */
	protected static $entries = [
		self::FR  				    => ['libelle' => 'Français', 'drapeau' => 'flag_fr.png'],
		self::EN 		        	=> ['libelle' => 'Anglais', 'drapeau' => 'flag_en.png'],
		self::DE 		        	=> ['libelle' => 'Allemand', 'drapeau' => 'flag_de.png'],
		self::BZH 		        	=> ['libelle' => 'Breton', 'drapeau' => 'flag_bzh.png'],
		self::EO 		        	=> ['libelle' => 'Espéranto', 'drapeau' => 'flag_eo.png'],
	];

	/**
	 * @return array
	 */
	public static function getCodes(): array
	{
		return [
			self::FR,
		    self::EN,
	    	self::DE,
    		self::BZH,
	    	self::EO
		];
	}

	/**
	 * @return array
	 */
	public static function getLibellesByKeys(): array
	{
		$roles = array();

		foreach (self::all() as $key => $type) {
			$type[$key] = $type['libelle'];
		}
		
		return $roles;
	}

	/**
	 * @param string $code
	 * 
	 * @return int
	 */
	public static function getDrapeauByCode(string $code): string
	{
		return self::$entries[$code]['drapeau'];
	}

	/**
	 * @return string
	 */
	public static function getLibelleByCode(string $code): string
	{
		return self::$entries[$code]['libelle'];
	}

	/**
	 * @return array
	 */
	public static function all(): array
	{
		return self::$entries;
	}
}