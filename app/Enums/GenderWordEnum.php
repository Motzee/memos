<?php

namespace App\Enums ;

abstract class GenderWordEnum {

	const MASCULIN = "masc";
	const FEMININ = "fem";
    const NEUTRE = "neutre";
	
	

	/** @var array user friendly named roles */
	protected static $entries = [
		self::MASCULIN  				        	=> ['libelle' => 'masculin', 'picto' => '♂'],
		self::FEMININ  		        	            => ['libelle' => 'féminin', 'picto' => '♀'],
		self::NEUTRE  			        	        => ['libelle' => 'neutre', 'picto' => '◍']
	];

	/**
	 * @return array
	 */
	public static function getCodes(): array
	{
		return [
			self::MASCULIN,
		    self::FEMININ,
	    	self::NEUTRE
		];
	}

	/**
	 * @return array
	 */
	public static function getLibellesByKeys(): array
	{
		$roles = array();

		foreach (self::all() as $key => $type) {
			$type[$key] = $type['libelle'];
		}
		
		return $roles;
	}

	/**
	 * @param string $code
	 * 
	 * @return int
	 */
	public static function getPictoByCode(string $code): string
	{
		return self::$entries[$code]['picto'];
	}

	/**
	 * @return string
	 */
	public static function getLibelleByCode(string $code): string
	{
		return self::$entries[$code]['libelle'];
	}

	/**
	 * @return array
	 */
	public static function all(): array
	{
		return self::$entries;
	}

	
}