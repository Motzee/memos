<?php

namespace App\Models;

use App\Models\User;
use App\Models\Domaine;
use App\Models\Memocateg;
use App\Models\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Memo extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'memos';

    /**
    * column in DB used for URLs
    */
    public function getRouteKeyName(){
        return "skey";
    }

    protected $fillable = [
        'id_user',
        'id_memocategs',
        'title',
        'slug',
        'domain', 
        'skey',
        'path',
        'numero',
        'is_public'
    ] ;

    public function getPath() {
        $subDomain = Domaine::where([['slug', '=', $this->domain]])->first() ;
        $parentDomain = Domaine::where([['id', '=', $subDomain->id_domaine_parent]])->first() ;

        $subCateg = Memocateg::where([['id', '=', $this->id_memocategs]])->first() ;
        $parentCateg = Memocateg::where([['id', '=', $subCateg->id_memocategs_parent]])->first() ;

        $path = $parentDomain->slug . "_". $this->domain . DIRECTORY_SEPARATOR. $parentCateg->skey . "_". $subCateg->skey . DIRECTORY_SEPARATOR. $this->path ;
        return $path ;
    }

    /* --------------------- RELATIONS --------------------- */

    /* RELATION ManyMemos To OneUser */
    public function user() { 
        return $this->belongsTo(User::class,'id_user','id'); 
    }

    /* RELATION ManyMemos To OneMemocateg */
    public function memocateg() { 
        return $this->belongsTo(Memocateg::class,'id_memocategs','id'); 
    }

    /* RELATION OneMemo To ManyReferences */
    public function refs() {
        return $this->hasMany(Reference::class, 'id_memo', 'id');
    }
}
