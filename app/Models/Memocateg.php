<?php

namespace App\Models;

use App\Models\Memo;
use App\Models\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Memocateg extends Model
{
    use HasFactory;
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'memocategs';

   protected $fillable = [
    'id_user',
    'id_memocategs_parent',
    'label',
    'slug',
    'skey',
    'domain',
    'path',
    'numero',
    'is_public'
] ;

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'id_memocategs_parent'=> null,
        'is_public'=> true
    ];

   /**
    * column in DB used for URLs
    */
    public function getRouteKeyName(){
        return "slug";
    }


    /* --------------------- RELATIONS --------------------- */

    /* RELATION ManyMemocategs To OneUser */
    public function user() { 
        return $this->belongsTo('User::class','id_user','id'); 
    }

    /* RELATION ManyMemocategChild To OneMemocategParent */
    public function parentMemocateg() { 
        return $this->belongsTo('Memocateg::class','id_memocategs_parent','id'); 
    }

    /* RELATION OneMemocateg To ManyMemos */
    public function memos() {
        return $this->hasMany(Memo::class, 'id_memocategs', 'id')->orderBy('numero');
    }

    /* RELATION OneMemocateg To ManyMemocategs */
    public function childrenCategs() {
        return $this->hasMany(Memocateg::class, 'id_memocategs_parent', 'id')->orderBy('numero');
    }

    /* RELATION OneMemocateg To ManyReferences */
    public function refs() {
        return $this->hasMany(Reference::class, 'id_memocateg', 'id');
    }

    
}
