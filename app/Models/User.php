<?php

namespace App\Models;

use App\Models\Memo;
use App\Models\Memocateg;
use App\Models\Reference;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /* --------------------- RELATIONS --------------------- */

    /* RELATION OneUser To ManyMemos */
    public function memos() {
        return $this->hasMany(Memo::class, 'id_user', 'id');
    }

    /* RELATION OneUser To ManyMemos */
    public function memocategs() {
        return $this->hasMany(Memocateg::class, 'id_user', 'id');
    }

    /* RELATION OneUser To ManyReferences */
    public function refs() {
        return $this->hasMany(Reference::class, 'id_user', 'id');
    }
}
