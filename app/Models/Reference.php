<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_user',
        'id_memocateg',
        'id_memo',
        'label',
        'url',
        'path_capture',
        'is_rss',
        'is_veille',
        'is_public',
        //'created_at',
        //'updated_at',
        //'deleted_at'
    ] ;

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'id_memocateg'=> null,
        'id_memo'=> null,
        'is_rss'=> false,
        'is_veille'=> false,
        'is_public'=> true
    ];
}
