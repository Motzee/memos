<?php

namespace App\Models;

use App\Models\Memocateg;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Domaine extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'domaines';


    protected $fillable = [
        'id_domaine_parent',
        'slug',
        'label',
        'type',
        'comments',
        'numero',
        'is_public'
    ] ;


    /**
    * column in DB used for URLs
    */
    public function getRouteKeyName(){
        return "slug";
    }


        /* --------------------- RELATIONS --------------------- */

    /* RELATION ManyMemocategChild To OneDomaineParent */
    public function parentDomaine() { 
        return $this->belongsTo(Domaine::class,'id_domaine_parent','id'); 
    }
    /* RELATION OneDomaine To ManyDomaines */
    public function childrenDomaines() {
        return $this->hasMany(Domaine::class, 'id_domaine_parent', 'id')->orderBy('numero');
    }

    public function parentCategs() {
        return $this->hasMany(Memocateg::class, 'domain', 'slug')->where([['id_memocategs_parent', "=", null]])->orderBy('numero'); //TODO ici à voir comment on le relie
    }
}
