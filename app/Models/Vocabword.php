<?php

namespace App\Models;

use App\Enums\GenderWordEnum;
use App\Enums\LanguageWordEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vocabword extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vocabulaire';

    /**
    * column in DB used for URLs
    */
    public function getRouteKeyName(){
        return "id";
    }

    protected $fillable = [
        'id_equivalent_fr',
        'lang',
        'word',
        'gender',
        'class', 
        'picture',
        'details',
        'level',
        'is_public'
    ] ;

    /* --------------------- RELATIONS --------------------- */

    /* RELATION ManyMemocategChild To OneMemocategParent */
    public function equivalentFR() { 
        return $this->belongsTo(Vocabword::class,'id_equivalent_fr','id'); 
    }

    /* RELATION OneMemocateg To ManyMemocategs */
    public function translations() {
        return $this->hasMany(Vocabword::class, 'id_equivalent_fr', 'id');
    }

    public function article(string $lang, ?string $genre = null) {
        if(is_null($genre)) {
            return "" ;
        }
        
        $articles = [
            "fr"    => [
                "masc"  => "le",
                "fem"    => "la",
                "ellision"  => "l'",
                "plur"  => "les"
            ],
            "en"    => [
                "masc"  => "the",
                "fem"    => "the",
                "plur"  => "the"
            ],
            "de"    => [
                "masc"  => "der",
                "fem"    => "die",
                "neutre"  => "das",
                "plur"  => "die"
            ],
            "bzh"   => [],
            "eo"    => []
        ] ;

        if($lang === LanguageWordEnum::FR && in_array($this->word[0], ['a', 'e', 'i', 'o', 'u', 'y'])) {
            //TODO encore une règle manquante pour les mots qui commencent par un h muet (avec exceptions donc)
            return "l'" ;
        } else {
            return $articles[$lang][$genre] ;
        }
    }

    /* RELATION Many to Many */
    public function wordsOfThisTheme() {
        return $this->belongsToMany(Vocabword::class, 'vocab_theme', 'id_wordtheme', 'id_word');
    }

    public function themesOfThisWord() {
        return $this->belongsToMany(Vocabword::class, 'vocab_theme', 'id_word', 'id_wordtheme');
    }
}
