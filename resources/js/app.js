import './bootstrap';

//import tailwind
import * as te from "tw-elements";

import Alpine from 'alpinejs';
import {slugifier} from './lib/slugifier.js' ;

window.Alpine = Alpine;

Alpine.start();

//imprt FLowbite (extension tailwind pour carousel)
import 'flowbite';


console.log('start') ;
/* Récupération des titres des 3 premiers niveaux après le titre principal dans le conteneur principal */
let titles = document.querySelector("#main").querySelectorAll('h2, h3, h4') ;
//console.debug(titles);

/* remplissage de la zone servant au sommaire */
let summary = document.querySelector("#summary");
if(summary !== null) {
    let uls = [] ;
    uls[0] = summary ;

    for (let i = 0 ; i < titles.length ; i++) {
        let aTitle = titles[i] ;
        //aTitle.id = 
        aTitle.id = slugifier(aTitle.textContent) ;
        let level = parseInt(aTitle.tagName.replace('H', '')) - 1 ;
        let li = document.createElement('li') ;
        let a = document.createElement('a') ;
        let theId = aTitle.id ;
        a.setAttribute('href', '#' + theId) ;
        a.textContent = aTitle.textContent ;
        li.appendChild(a) ;
        //
        if(!uls[level - 1]) {
            let subUl = document.createElement('ul') ;
            uls[level - 1] = subUl ;
            uls[level - 2].lastChild.appendChild(subUl) ;
        }
        //--
        uls[level] = null ; 
        uls[level - 1].appendChild(li) ;
    }
}

console.log('end') ;