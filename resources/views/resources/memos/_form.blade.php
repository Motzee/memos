<form
    action="@if($method === 'PUT'){{ route('memos.update', ['memo' => $memo->skey]) }}@else{{ route('memos.store')}}@endif" method="post">

    @csrf
    @if($method === 'PUT')
        @method('PUT')
    @endif
    
    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="title" required>Titre</label>
        </div>
        <div class="form_col-75">
            <input id="title" name="title" type="text" value="{{ $memo->title }}"
                class="form-input @error('title') is-invalid @enderror" maxlength="255" required />
        </div>
        @error('title')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="slug" required>SKey</label>
        </div>
        <div class="form_col-75">
            <input id="skey" name="skey" type="text" value="{{ $memo->skey }}"
                class="form-input @error('skey') is-invalid @enderror" maxlength="255" required />
        </div>
        @error('skey')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="domain" required>Sous-domaine</label>
        </div>
        <div class="form_col-75"><select name="domain" id="domain">
            <option></option>
            @foreach ($domains as $item)
                <optgroup label="{{ $item->label}}">
                    @foreach ($item->childrenDomaines as $subD)
                        <option {{ $subD->slug === $memo->domain ? 'selected' : '' }} value="{{ $subD->slug }}">{{ $subD->label }}</option>
                    @endforeach
                    
                </optgroup>
            @endforeach
            </select></div>
        @error('domain')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="id_memocategs" required>Sous-catégorie</label>
        </div>
        <div class="form_col-75"><select name="id_memocategs" id="id_memocategs">
            <option></option>
            @foreach ($categs as $item)
                <optgroup label="{{ $item->label}}">
                    @foreach ($item->childrenCategs as $subC)
                        <option {{ $subC->id === $memo->id_memocategs ? 'selected' : '' }} value="{{ $subC->id }}">{{ $subC->label }}</option>
                    @endforeach
                    
                </optgroup>
            @endforeach
            </select></div>
        @error('id_memocategs')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="path" required>Nom de fichier (sans l'extension)</label>
        </div>
        <div class="form_col-75">
            <input id="path" name="path" type="text" value="{{ $memo->path }}"
                class="form-input @error('path') is-invalid @enderror" maxlength="255" required placeholder="00_nomFichierSansExt" />
        </div>
        @error('path')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25"><label for="numero" required>Ordre (numéro)</label></div>
        <div class="form_col-75"><input id="numero" name="numero" value="{{ $memo->numero }}" type="number"
                class="form-input" required /></div>
        @error('numero')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25"></div>
        <div class="form_col-75">
            <input type="checkbox" name="is_public" {{ $memo->is_public === 1 ? 'checked' : '' }} id="is_public" value="1" />
            <label for="is_public">Visibilité publique</label></div>
        @error('is_public')
            {{ $message }}
        @enderror
    </div>


    <input class="button validation" type="submit" value="Envoyer" />
</form>
