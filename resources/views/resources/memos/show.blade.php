<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => $menuContent,
        ])
    </x-slot>
    
    <x-slot name="header">
        <nav class="ariane">
            <a href="{{ route('memocategs.show2', ['slugSubDomaine' => $memo->domain, 'slugCateg' => $parentCategs[0]['slug']]) }}"> {{ $parentCategs[0]['label'] }}</a> > 
            <a href="{{ route('memocategs.show2', ['slugSubDomaine' => $memo->domain, 'slugCateg' =>$parentCategs[1]['slug']]) }}"> {{ $parentCategs[1]['label'] }}</a>
        </nav>

        <div class="d-flex justify-between items-center">
            <h1 class="font-semibold text-xl leading-tight">
                {{ $memo->title }}
            </h1>
            @if (Auth::user() !== null && $memo->id_user === Auth::user()->id)
                <a class="button" href="{{ route('memos.edit', ['memo' => $memo->skey]) }}">Modifier</a>
            @endif
        </div>

        @if ($memo->is_public === 0)
            <p class="text-warning">Ce mémo privé n'est visible que par vous</p>
        @endif
    </x-slot>

    <aside class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <span class="txt_catchy">Sommaire cliquable</span>
            <ul id="summary"></ul>
        </div>
    </aside>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('memos.' . $memo->getPath()) 
        </div>
    </div>

    @if (count($memo->refs) > 0 || Auth::user() )
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2>Ressources disponibles</h2>
            @auth
                <a href="{{ route('references.create', ['id_memo' => $memo->id]) }}" target="_blank" rel="noopener noreferrer">Ajouter</a>
            @endauth

            @include('resources.references._listing',
            [
                'references' => $memo->refs
            ]
        )
        </div>
    </div>        
    @endif
    
</x-app-layout>
