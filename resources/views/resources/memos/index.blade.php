<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => [],
        ])
    </x-slot>

    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            {{ __('Liste des Mémos') }}
        </h1>
        @if(Auth::user() !== null)
            <a href="{{ route('memos.create') }}">Ajouter</a>
        @endif
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @foreach ($categs as $aCateg)
                @isset($aCateg['memos'])
                    <section class="mb-4">
                        <h2>{{ $aCateg['label'] }}</h2>
                        <ul>
                            @foreach ($aCateg['memos'] as $aMemo)
                                <li class="{{ $aMemo->is_public === 0 ? 'is_private' : '' }}"><a href="{{ route('memos.show2', ['subDomain' => $aMemo->domain, 'subCateg' => $aMemo->memocateg->slug, 'memo' => $aMemo->skey]) }}">{{ $aMemo->title }}</a></li>
                            @endforeach
                        </ul>
                    </section>
                @endisset
            @endforeach
        </div>
    </div>
</x-app-layout>
