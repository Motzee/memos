<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => $menuContent,
        ])
    </x-slot>
    
    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            Modification de "{{ $memo->title }}"
        </h1>
        <form method="POST" action="{{ route('memos.destroy', $memo) }}">
            @csrf
            @method('DELETE')
            <button class="button dangerous" type="submit">Supprimer</button>
        </form>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('resources.memos._form',
                [
                    'method' => 'PUT'
                ]
            )
        </div>
    </div>
</x-app-layout>