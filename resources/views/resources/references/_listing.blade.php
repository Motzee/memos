<ul>
    @foreach ($references as $aRef)
        <li>
            @if(!is_null($aRef->url))
                <a href="{{ $aRef->url }}" target="_blank" rel="noopener noreferrer">{{ $aRef->label }}</a>
            @else
                {{ $aRef->label }}
            @endif
            @auth
            <a href="{{ route('references.edit', $aRef->id) }}" target="_blank" rel="noopener noreferrer">Modifier</a>
            @endauth
        </li>
    @endforeach
</ul>