<form
    action="@if($method === 'PUT'){{ route('references.update', ['reference' => $ref->id]) }}@else{{ route('references.store')}}@endif" method="post">

    @csrf
    @if($method === 'PUT')
        @method('PUT')
    @endif
    
    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="label" required>Titre</label>
        </div>
        <div class="form_col-75">
            <input id="label" name="label" type="text" value="{{ $ref->label }}"
                class="form-input @error('label') is-invalid @enderror" maxlength="255" required />
        </div>
        @error('label')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="url">Url</label>
        </div>
        <div class="form_col-75">
            <input id="url" name="url" type="text" value="{{ $ref->url }}"
                class="form-input @error('url') is-invalid @enderror" maxlength="255" />
        </div>
        @error('url')
            {{ $message }}
        @enderror
    </div>

    <fieldset>
        <legend required>Rattachement</legend>
        <p>De préférence à une seule chose parmi catégorie ou mémo…</p>

        <div class="form_row mb-4">
            <div class="form_col-25"><label for="id_memocateg">Id de la catégorie</label></div>
            <div class="form_col-75"><input id="id_memocateg" name="id_memocateg" value="{{ $ref->id_memocateg }}" type="number"
                    class="form-input" /></div>
            @error('id_memocateg')
                {{ $message }}
            @enderror
        </div>
    
        <div class="form_row mb-4">
            <div class="form_col-25"><label for="id_memo">ID du mémo</label></div>
            <div class="form_col-75"><input id="id_memo" name="id_memo" value="{{ $ref->id_memo }}" type="number"
                    class="form-input" /></div>
            @error('id_memo')
                {{ $message }}
            @enderror
        </div>
    </fieldset>
    

    <div class="form_row mb-4">
        <div class="form_col-25"></div>
        <div class="form_col-75">
            <input type="checkbox" name="is_rss" {{ $ref->is_rss === 1 ? 'checked' : '' }} id="is_rss" value="1" />
            <label for="is_rss">Flux RSS</label></div>
        @error('is_rss')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25"></div>
        <div class="form_col-75">
            <input type="checkbox" name="is_veille" {{ $ref->is_veille === 1 ? 'checked' : '' }} id="is_veille" value="1" />
            <label for="is_veille">Site pour Veille</label></div>
        @error('is_veille')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25"></div>
        <div class="form_col-75">
            <input type="checkbox" name="is_public" {{ $ref->is_public === 1 ? 'checked' : '' }} id="is_public" value="1" />
            <label for="is_public">Visibilité publique</label></div>
        @error('is_public')
            {{ $message }}
        @enderror
    </div>

    <input class="button validation" type="submit" value="Envoyer" />
</form>
