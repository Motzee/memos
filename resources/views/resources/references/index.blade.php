<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => [],
        ])
    </x-slot>

    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            {{ __('Liste des Références') }}
        </h1>

        @auth
            <a href="{{ route('references.create') }}">Ajouter</a>
        @endauth 
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (count($references) === 0 )
            <p>Aucune référence n'est disponible en base de données pour votre statut actuellement.</p>
            @else
                @include('resources.references._listing',
                    [
                        'references' => $references
                    ]
                )
            @endif
        </div>
    </div>
</x-app-layout>
