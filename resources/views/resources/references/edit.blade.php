<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => [],
        ])
    </x-slot>
    
    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            Modification d'une référence
        </h1>
        <form method="POST" action="{{ route('references.destroy', $ref) }}">
            @csrf
            @method('DELETE')
            <button class="button dangerous" type="submit">Supprimer</button>
        </form>

    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('resources.references._form',
                [
                    'method' => 'PUT'
                ]
            )
        </div>
    </div>
</x-app-layout>