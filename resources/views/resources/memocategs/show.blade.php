<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => $menuContent,
        ])
    </x-slot>

    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            {{ $categ->label }}
        </h1>
        @auth
            <a href="{{ route('memocategs.edit', $categ->slug) }}">Modifier</a>
        @endauth
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (count($categ->childrenCategs) > 0)
                <h2>Sous-catégories disponibles</h2>
                @foreach ($categ->childrenCategs as $aSubCateg)
                    @if (count($aSubCateg->memos) > 0)
                        <section class="py-2">
                            <h3><a href="{{ route('memocategs.show', $aSubCateg->slug) }}">{{ $aSubCateg->label }}</a>
                            </h3>
                            @if (count($aSubCateg->memos) > 0)
                                <ul>
                                    @foreach ($aSubCateg->memos as $aMemo)
                                        <li class="{{ $aMemo->is_public === 0 ? 'is_private' : '' }}"><a
                                                href="{{ route('memos.show2', ['subDomain' => $aMemo->domain, 'subCateg' => $aSubCateg->slug, 'memo' => $aMemo->skey]) }}">{{ $aMemo->title }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </section>
                    @endif
                @endforeach
            @endif

            <section>
                <h2>Mémos associés</h2>
                @auth
                    <a href="{{ route('memos.create', ['id_memocateg' => $categ->id]) }}" target="_blank"
                        rel="noopener noreferrer">Ajouter</a>
                @endauth
                @if (count($categ->memos) > 0)

                    <ul>
                        @foreach ($categ->memos as $aMemo)
                            <li class="{{ $aMemo->is_public === 0 ? 'is_private' : '' }}"><a
                                    href="{{ route('memos.show2', ['subDomain' => $aMemo->domain, 'subCateg' => $categ->slug, 'memo' => $aMemo->skey]) }}">{{ $aMemo['title'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </section>
        </div>
    </div>

    @if (count($categ->refs) > 0 || Auth::user())
        <div class="py-4">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <h2>Ressources disponibles</h2>
                @auth
                    <a href="{{ route('references.create', ['id_memocateg' => $categ->id]) }}" target="_blank"
                        rel="noopener noreferrer">Ajouter</a>
                @endauth

                @include('resources.references._listing', [
                    'references' => $categ->refs,
                ])
            </div>
        </div>
    @endif
</x-app-layout>
