<form
    action="@if ($method === 'PUT') {{ route('memocategs.update', ['memocateg' => $memocateg->slug]) }}@else{{ route('memocategs.store') }} @endif"
    method="post">

    @csrf
    @if ($method === 'PUT')
        @method('PUT')
    @endif

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="label" required>Label</label>
        </div>
        <div class="form_col-75">
            <input id="label" name="label" type="text" value="{{ $memocateg->label }}"
                class="form-input @error('label') is-invalid @enderror" maxlength="255" required />
        </div>
        @error('label')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="slug" required>Slug</label>
        </div>
        <div class="form_col-75">
            <input id="slug" name="slug" type="text" value="{{ $memocateg->slug }}"
                class="form-input @error('slug') is-invalid @enderror" maxlength="255" required />
        </div>
        @error('slug')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="slug" required>SKey</label>
        </div>
        <div class="form_col-75">
            <input id="skey" name="skey" type="text" value="{{ $memocateg->skey }}"
                class="form-input @error('skey') is-invalid @enderror" maxlength="255" required />
        </div>
        @error('skey')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="domain" required>Sous-Domaine</label>
        </div>

        <div class="form_col-75"><select name="domain" id="domain">
                <option></option>
                @foreach ($domaines as $aParentDomaine)
                    <optgroup label="{{ $aParentDomaine->label }}">
                        @foreach ($aParentDomaine->childrenDomaines as $aSubDomain)
                            <option {{ $memocateg->domain === $aSubDomain->slug ? 'selected' : '' }}
                                value="{{ $aSubDomain->slug }}">{{ $aSubDomain->label }}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
        @error('domain')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="id_memocategs_parent">Catégorie parente éventuelle</label>
        </div>
        <div class="form_col-75"><select name="id_memocategs_parent" id="id_memocategs_parent">
                <option></option>
                @foreach ($parentCategs as $aParentCateg)
                    <option {{ $aParentCateg->id === $memocateg->id_memocategs_parent ? 'selected' : '' }}
                        value="{{ $aParentCateg->id }}">{{ $aParentCateg->label }}</option>
                @endforeach
            </select>
        </div>
        @error('id_memocategs_parent')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="path" required>Nom de dossier</label>
        </div>
        <div class="form_col-75">
            <input id="path" name="path" type="text" value="{{ $memocateg->path }}"
                class="form-input @error('path') is-invalid @enderror" maxlength="255" required />
        </div>
        @error('path')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25"><label for="numero" required>Ordre (numéro)</label></div>
        <div class="form_col-75"><input id="numero" name="numero" value="{{ $memocateg->numero }}" type="number"
                class="form-input" required /></div>
        @error('numero')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25"></div>
        <div class="form_col-75">
            <input type="checkbox" name="is_public" {{ $memocateg->is_public === 1 ? 'checked' : '' }} id="is_public"
                value="1" />
            <label for="is_public">Visibilité publique</label>
        </div>
        @error('is_public')
            {{ $message }}
        @enderror
    </div>


    <input class="button validation" type="submit" value="Envoyer" />
</form>
