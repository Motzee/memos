<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => [],
        ])
    </x-slot>


    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            {{ __('Liste des Catégories de mémos') }}
        </h1>
        @auth
            <a href="{{ route('memocategs.create') }}">Ajouter</a>
        @endauth
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @foreach ($parentDomaines as $aDomaine)
            <h2>{{ $aDomaine->label }}</h2>
            @foreach ($aDomaine->childrenDomaines as $aChildDomain)
                <section>
                    <h3>{{ $aChildDomain->label }}</h3>
                    @foreach ($aChildDomain->parentCategs as $aParentCateg)
                        <section>
                            <h4><a href="{{ route('memocategs.show', $aParentCateg->slug) }}" title="id {{ $aParentCateg->id }}">{{ $aParentCateg->label }}</a></h4>
                            @foreach ($aParentCateg->childrenCategs as $aChildCateg)
                            <section>
                                <h5><a href="{{ route('memocategs.show', $aChildCateg->slug) }}" title="id {{ $aChildCateg->id }}">{{ $aChildCateg->label }}</a></h5>
                            </section>
                        @endforeach
                        </section>
                    @endforeach
                </section>
            @endforeach
            @endforeach
        </div>
    </div>
</x-app-layout>
