<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => $menuContent,
        ])
    </x-slot>

    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            {{ $data->label }}
        </h1>
        @auth
            <a href="{{ route('memos.create') }}">Ajouter</a>
        @endauth
    </x-slot>

    <div class="pt-4 pb-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            @if (str_starts_with($data->slug, 'lang_'))
            <a href="{{ route('vocab.devine') }}">Flashcards de vocabulaire</a>
            @endif

            {{-- Si on est dans une catégorie qui a des sous-categs : on les liste --}}
            @foreach ($data->childrenCategs as $key => $aSubCateg)
                <section class="mb-4">
                    <h2><a href="{{ route('memocategs.show2', ['slugSubDomaine' => $aSubCateg->domain, 'slugCateg' => $aSubCateg->slug]) }}"
                                title="id : {{ $aSubCateg->id }}">{{ $aSubCateg->label }}</a> <span class="text-small"></span>
                        </h2>
                        <ul>
                            @foreach ($aSubCateg->memos as $subKey => $aMemo)
                                @if ($aMemo->is_public === 1 || (Auth::user() && $aMemo->id_user === Auth::user()->id))
                                    <li class="{{ $aMemo->is_public === 0 ? 'is_private' : '' }}"><a
                                            href="{{ route('memos.show2', ['subDomain' => $aSubCateg->domain, 'subCateg' => $aSubCateg->slug, 'memo' => $aMemo->skey]) }}">{{ $aMemo->title }}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                </section>
            @endforeach

            {{-- Si on est dans une sous-categ : on en liste les mémos --}}
            <ul>
                @foreach ($data->memos as $key => $aMemo)
                    @if ($aMemo->is_public === 1 || (Auth::user() && $aMemo->id_user === Auth::user()->id))
                        <li class="{{ $aMemo->is_public === 0 ? 'is_private' : '' }}">
                            <a href="{{ route('memos.show2', ['subDomain' => $data->domain, 'subCateg' => $data->slug, 'memo' => $aMemo->skey]) }}">
                                {{ $aMemo->title }}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>

            @if (count($data->refs) > 0 || Auth::user())
                <div class="py-4">
                    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                        <h3>Ressources disponibles</h3>
                        @auth
                            <a href="{{ route('references.create', ['id_memocateg' => $data->id]) }}" target="_blank"
                                rel="noopener noreferrer">Ajouter</a>
                        @endauth

                        @include('resources.references._listing', [
                            'references' => $data->refs,
                        ])
                    </div>
                </div>
            @endif
        </div>
    </div>
</x-app-layout>
