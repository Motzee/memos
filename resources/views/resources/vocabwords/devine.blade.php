<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => [],
        ])
    </x-slot>

    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            {{ __('Devine les mots suivants') }}
        </h1>
    </x-slot>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const responses = document.getElementById('traductions')
            const showResponse = function() {
                console.log(responses)
            }
        });
    </script>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (count($themes) === 0)
                <p>Aucun thème n'est disponible en base de données pour votre statut actuellement.</p>
            @else
            <div class="flex">
                <div class="d-flex flex-col justify-center items-center">
                    <img class="vignette" src="" alt="" />
                    <span>Tous</span>
                </div>
                @foreach ($themes as $aTheme)
                <div class="d-flex flex-col justify-center items-center">
                    <img class="vignette" src="{{ $aTheme['picture'] }}" alt="" />
                    <span>{{ $aTheme['word'] }}</span>
                </div>
                @endforeach
                <div class="d-flex flex-col justify-center items-center">
                    <img class="vignette" src="" alt="" />
                    <span>sans theme</span>
                </div>
            @endif
            {{--
            @if (count($words) === 0)
                <p>Aucun mot n'est disponible en base de données pour votre statut actuellement.</p>
            @else
                <div id="default-carousel" class="relative w-full" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative overflow-hidden rounded-lg" style="height:calc(100vh - 137px - 48px * 2)">
                        <!-- Items-->
                        
                        @foreach ($words as $aWord)
                            <div class="hidden duration-700 ease-in-out flex flex-col items-center" data-carousel-item>
                                <p><img class="xxs-img pr-2 inline"
                                        src="/img/icons/{{ App\Enums\LanguageWordEnum::getDrapeauByCode($aWord['lang']) }}"
                                        alt="{{ App\Enums\LanguageWordEnum::getDrapeauByCode($aWord['lang']) }}" />
                                        {{ $aWord->article($aWord->lang, $aWord->gender) }} {{ $aWord['word'] }}
                                </p>
                                <img src="/img/flashcards/{{ $aWord->picture }}" class="flashcard block" alt="">
                                <button class="btn btn-normal" onclick="showResponse()" style="z-index : 2">Voir la
                                    traduction</button>
                                <div id="traductions">
                                    @foreach ($aWord['translations'] as $aTranslation)
                                        <p>
                                            <img class="xxs-img pr-2 inline"
                                                src="/img/icons/{{ App\Enums\LanguageWordEnum::getDrapeauByCode($aTranslation['lang']) }}"
                                                alt="{{ App\Enums\LanguageWordEnum::getDrapeauByCode($aTranslation['lang']) }}" />
                                            {{ $aTranslation->article($aTranslation->lang, $aTranslation->gender) }} {{ $aTranslation['word'] }}
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                        
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1"
                            data-carousel-slide-to="0"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 2"
                            data-carousel-slide-to="1"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 3"
                            data-carousel-slide-to="2"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 4"
                            data-carousel-slide-to="3"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 5"
                            data-carousel-slide-to="4"></button>
                    </div>
                    <!-- Slider controls -->
                    <button type="button"
                        class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                        data-carousel-prev>
                        <span
                            class="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                            <svg class="w-4 h-4 text-black dark:text-gray-800" aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2" d="M5 1 1 5l4 4" />
                            </svg>
                            <span class="sr-only">Previous</span>
                        </span>
                    </button>
                    <button type="button"
                        class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                        data-carousel-next>
                        <span
                            class="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                            <svg class="w-4 h-4 text-black dark:text-gray-800" aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2" d="m1 9 4-4-4-4" />
                            </svg>
                            <span class="sr-only">Next</span>
                        </span>
                    </button>
                </div>
            @endif
            --}}
        </div>
    </div>
</x-app-layout>
