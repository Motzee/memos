<form
    action="@if($method === 'PUT'){{ route('vocabwords.update', ['word' => $word->id]) }}@else{{ route('vocabwords.store')}}@endif" method="post">

    @csrf
    @if($method === 'PUT')
        @method('PUT')
    @endif
    

    <div class="form_row mb-4">
        <div class="form_col-25"><label for="id_equivalent_fr">Id du mot FR équivalent</label></div>
        <div class="form_col-75"><input id="id_equivalent_fr" name="id_equivalent_fr" value="{{ $word->id_equivalent_fr }}" type="number"
                class="form-input" /></div>
        @error('id_equivalent_fr')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="word" required>Mot</label>
        </div>
        <div class="form_col-75">
            <input id="word" name="word" type="text" value="{{ $word->word }}"
                class="form-input @error('word') is-invalid @enderror" maxlength="255" required />
        </div>
        @error('word')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="lang" required>Langue</label>
        </div>

        <div class="form_col-75"><select name="lang" id="lang">
                <option></option>
                @foreach (App\Enums\LanguageWordEnum::all() as $key => $aLang)
                    <option {{ $word->lang === $aLang['libelle'] ? 'selected' : '' }}
                                value="{{ $key }}">{{ $aLang['libelle'] }}</option>
                @endforeach
            </select>
        </div>
        @error('lang')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="gender" required>Genre</label>
        </div>

        <div class="form_col-75"><select name="gender" id="gender">
                <option></option>
                @foreach (App\Enums\GenderWordEnum::all() as $key => $aGender)
                    <option {{ $word->gender === $aGender['libelle'] ? 'selected' : '' }}
                                value="{{ $key }}">{{ $aGender['libelle'] }}</option>
                @endforeach
            </select>
        </div>
        @error('gender')
            {{ $message }}
        @enderror
    </div>

    <div class="form_row mb-4">
        <div class="form_col-25">
            <label for="class" required>Classe grammaticale</label>
        </div>

        <div class="form_col-75"><select name="class" id="class">
                <option></option>
                @foreach (App\Enums\ClassWordEnum::all() as $key => $aClass)
                    <option {{ $word->class === $aClass['libelle'] ? 'selected' : '' }}
                                value="{{ $key }}">{{ $aClass['libelle'] }}</option>
                @endforeach
            </select>
        </div>
        @error('class')
            {{ $message }}
        @enderror
    </div>


    <div class="form_row mb-4">
        <div class="form_col-25"></div>
        <div class="form_col-75">
            <input type="checkbox" name="is_public" {{ $word->is_public === 1 ? 'checked' : '' }} id="is_public" value="1" />
            <label for="is_public">Visibilité publique</label></div>
        @error('is_public')
            {{ $message }}
        @enderror
    </div>

    <input class="button validation" type="submit" value="Envoyer" />
</form>
