<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => [],
        ])
    </x-slot>

    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            {{ __('Liste des Mots de vocabulaire') }}
        </h1>

        @auth
            <a href="{{ route('vocabwords.create') }}">Ajouter</a>
        @endauth
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (count($words) === 0)
                <p>Aucun mot n'est disponible en base de données pour votre statut actuellement.</p>
            @else
                {{ $words->links() }}
                <table>
                    <thead>
                        <tr>
                            <th scope="col" id="col_year">id</th>
                            <th scope="col" id="" abbr="">Flashcard</th>
                            <th scope="col" id="col_name">Mot</th>
                            <th scope="col" id="">Traductions</th>
                            <th scope="col" id="">Niveau</th>
                            <th scope="col" id="" abbr="Classe">Classe grammaticale</th>
                            <th scope="col" id="">Détails</th>
                            <th scope="col" id=""></th>

                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($words as $aWord)
                            <tr class="">
                                <td headers="col_" rowspan="{{ $aWord->translations->count() === 0 ? 1 : $aWord->translations->count() }}">{{ $aWord->id }}
                                </td>
                                <td headers="col_" rowspan="{{ $aWord->translations->count() === 0 ? 1 : $aWord->translations->count() }}"><img class="vignette" src="{{ '/img/flashcards/' . $aWord->picture }}" alt="" />
                                </td>
                                <td headers="col_" rowspan="{{ $aWord->translations->count() === 0 ? 1 : $aWord->translations->count() }}">
                                    {{ !is_null($aWord->gender) ? App\Enums\GenderWordEnum::getPictoByCode($aWord->gender) : '' }} {{ $aWord->word }}
                                </td>
                                <td headers="col_">
                                    @if ($aWord->translations && count($aWord->translations) >= 1)
                                        <div class="d-flex justify-content-start align-items-center">
                                            <img class="xxs-img pr-2"
                                                src="/img/icons/{{ App\Enums\LanguageWordEnum::getDrapeauByCode($aWord->translations[0]->lang) }}"
                                                alt="{{ App\Enums\LanguageWordEnum::getDrapeauByCode($aWord->translations[0]->lang) }}" />
                                            <span>{{ $aWord->translations[0]->word }}</span>
                                        </div>
                                    @endif

                                </td>
                                <td headers="col_" rowspan="{{ $aWord->translations->count() === 0 ? 1 : $aWord->translations->count() }}">{{ $aWord->level }}
                                </td>
                                <td headers="col_" rowspan="{{ $aWord->translations->count() === 0 ? 1 : $aWord->translations->count() }}">{{ $aWord->class }}
                                </td>
                                <td headers="col_" rowspan="{{ $aWord->translations->count() === 0 ? 1 : $aWord->translations->count() }}">{{ $aWord->details }}
                                </td>
                                <td headers="col_" rowspan="{{ $aWord->translations->count() === 0 ? 1 : $aWord->translations->count() }}"></td>
                            </tr>
                            @if ($aWord->translations && count($aWord->translations) > 1)
                                @foreach ($aWord->translations as $aTranslation)
                                    @if (!$loop->first)
                                        <tr>
                                            <td>
                                                <div class="d-flex justify-content-start align-items-center">
                                                    <img class="xxs-img pr-2"
                                                        src="/img/icons/{{ App\Enums\LanguageWordEnum::getDrapeauByCode($aTranslation->lang) }}"
                                                        alt="{{ App\Enums\LanguageWordEnum::getDrapeauByCode($aTranslation->lang) }}" />
                                                    <span>{{ $aTranslation->word }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </tbody>
                </table>
                {{ $words->links() }}
            @endif
        </div>
    </div>
</x-app-layout>
