<x-app-layout>
    <x-slot name="navigation">
        @include('layouts.navigation', [
            'menuContent' => $menuContent
        ])
    </x-slot>
    
    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            Bienvenue sur Les Mémos de Motzee LaLicorne !
        </h1>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <p>Cette interface est la nouvelle version de mon carnet à mémos. Mes notes sont parfois prises à l'arrache côté mise en forme vu que le public cible c'est moi ^o^. Cette
                plateforme n'est accessible qu'en lecture si vous n'êtes pas authentifié·e.</p>

            <nav>
                
                <!--a href="{{ route('vocab.devine') }}">Flashcards de vocabulaire</a-->
                <!--a href="">Recettes</a-->
            </nav>

            <div id="accordion-collapse" data-accordion="collapse" class="py-4">
                @foreach ($domains as $aDomain)
                <h2 id="accordion-collapse-heading-{{ $aDomain->slug }}">
                    <button type="button" class="flex items-center justify-between w-full px-2 py-4 font-medium rtl:text-right text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800 gap-3" data-accordion-target="#accordion-collapse-body-{{ $aDomain->slug }}" aria-expanded="false" aria-controls="accordion-collapse-body-{{ $aDomain->slug }}">
                    <span>{{ $aDomain->label }}</span>
                    <svg data-accordion-icon class="w-3 h-3 rotate-180 shrink-0" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5 5 1 1 5"/>
                    </svg>
                    </button>
                </h2>
                <div id="accordion-collapse-body-{{ $aDomain->slug }}" class="hidden"      aria-labelledby="accordion-collapse-heading-{{ $aDomain->slug }}">
                    <div class="p-5 border border-b-0 border-gray-200 dark:border-gray-700">
                        <div class="py-4 px-5">
                            <ul>
                                @foreach ($subDomains[$aDomain->id] as $aSubdomain)
                                    <li><a href="{{ route('subdomain.index', $aSubdomain->slug) }}">{{ $aSubdomain->label }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</x-app-layout>
