<x-app-layout>
    <x-slot name="header">
        <h1 class="font-semibold text-xl leading-tight">
            {{ __('Les inscriptions sont actuellement fermées') }}
        </h1>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <p>Il n'est actuellement pas possible de s'inscrire, cependant aucun compte n'est nécessaire pour consulter
                ce site.</p>
            <p>Si vous pensiez néanmoins avoir besoin d'un compte personnel, contactez LaLicorne pour voir s'il est
                possible de faire rouvrir les inscriptions pour vous.</p>
        </div>
    </div>
</x-app-layout>
