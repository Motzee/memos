<nav x-data="{ open: false }" class="bg-mane border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="shrink-0 flex items-center">
                    <a href="{{ route('home') }}" title="Accueil des Mémos de LaLicorne">
                        <img class="sm-img" src="/img/icons/avatar_rarity_low.png" alt="Les mémos de LaLicorne">
                    </a>
                </div>
                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    @foreach ($menuContent as $key => $aParentCateg)
                       <x-nav-link :href="route('memocategs.show2', ['slugSubDomaine' => $aParentCateg['domain'], 'slugCateg' => $aParentCateg['slug']])">
                            {{ $aParentCateg['label'] }}
                        </x-nav-link>
                    @endforeach
                </div>

            </div>


            <div class="hidden sm:flex sm:items-center sm:ml-6">
                @if (Auth::user())
                    <!-- Settings Dropdown -->
                    <x-dropdown align="right" width="48">
                        <x-slot name="trigger">
                            <button
                                class="flex items-center text-sm font-medium hover:border-gray-300 focus:outline-none focus:border-gray-300 transition duration-150 ease-in-out">
                                <div>
                                    {{ Auth::user()->name }}
                                </div>
                                <div class="ml-1">
                                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 20 20">
                                        <path fill-rule="evenodd"
                                            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                            clip-rule="evenodd" />
                                    </svg>
                                </div>
                            </button>
                        </x-slot>

                        <x-slot name="content">
                            <nav>
                                <ul>
                                    <li>Domaines</li>
                                    <li><a href="{{ route('memocategs.index') }}">Catégories</a></li>
                                    <li><a href="{{ route('memos.index') }}">Mémos</a></li>
                                    <li><a href="{{ route('references.index') }}">Références</a></li>
                                    <li><a href="{{ route('vocabwords.index') }}">Vocabulaire</a></li>
                                </ul>
                            </nav>
                            <!-- Authentication -->
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <x-dropdown-link :href="route('logout')"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                    {{ __('Déconnexion') }}
                                </x-dropdown-link>
                            </form>
                        </x-slot>
                    </x-dropdown>
                @else
                    <a class="d-none" href="{{ route('login') }}">Se connecter</a>
                @endif
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open"
                    class="inline-flex items-center justify-center p-2 rounded-md hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                    title="ouvrir menu navigation">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24"
                        alt="ouvrir menu navigation">
                        <path :class="{ 'hidden': open, 'inline-flex': !open }" class="inline-flex"
                            stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{ 'hidden': !open, 'inline-flex': open }" class="hidden" stroke-linecap="round"
                            stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{ 'block': open, 'hidden': !open }" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <!-- Navigation Links -->
            @foreach ($menuContent as $key => $aParentCateg)
                <x-responsive-nav-link :href="route('memocategs.show2', ['slugSubDomaine' => $aParentCateg['domain'], 'slugCateg' => $aParentCateg['slug']])" {{-- :active="request()->routeIs('memocategs.show2') //TODO cibler au slug près" --}}>
                    {{ $aParentCateg['label'] }}
                </x-responsive-nav-link>
            @endforeach
        </div>

        <!-- Responsive Settings Options -->
        @if (Auth::user())   
            <div class="pt-4 pb-1 border-t border-gray-200">
                <div class="px-4">
                    <div class="font-medium text-base">{{ Auth::user()->name }}</div>
                    <div class="font-medium text-sm">{{ Auth::user()->email }}</div>
                </div>

                <div class="px-4">
                    <nav>
                        <ul>
                            <li><a class="block pl-3 pr-4 py-2 text-base" href="{{route('memos.index')}}">Mémos</a></li>
                            <li><a class="block pl-3 pr-4 py-2 text-base" href="{{route('memocategs.index')}}">Catégories</a></li>
                            <li><a class="block pl-3 pr-4 py-2 text-base" href="{{route('references.index')}}">Références</a></li>
                            <li><a class="block pl-3 pr-4 py-2 text-base" href="{{route('vocabwords.index')}}">Vocabulaire</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="space-y-1">
                    <!-- Authentication -->
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf

                        <x-responsive-nav-link :href="route('logout')"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                            {{ __('Déconnexion') }}
                        </x-responsive-nav-link>
                    </form>
                </div>
            </div>
        @else
            <div class="d-none px-4 py-2"><a href="{{ route('login') }}">Se connecter</a></div>
        @endif
    </div>
</nav>
