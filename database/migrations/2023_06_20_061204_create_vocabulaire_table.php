<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVocabulaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vocabulaire', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_equivalent_fr')->nullable();
            $table->foreign('id_equivalent_fr')->references('id')->on('vocabulaire');
   
            $table->string('lang', 16) ;
            $table->string('word', 255) ;
            $table->string('gender', 16)->nullable() ; //masculin, feminin, neutre
            $table->string('class', 64)->nullable() ; //voir enum
            $table->string('picture', 255)->nullable() ;
            $table->json('details')->nullable() ;
            $table->integer('level')->nullable() ;
            $table->boolean('is_public')->default(1) ;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vocabulaire');
    }
}
