<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVocabThemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vocab_theme', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_wordtheme')->nullable();
            $table->foreign('id_wordtheme')->references('id')->on('vocabulaire');

            $table->unsignedBigInteger('id_word')->nullable();
            $table->foreign('id_word')->references('id')->on('vocabulaire');

            //$table->json('details') ;
            $table->integer('level') ;
            $table->boolean('is_public')->default(1) ;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vocab_theme');
    }
}
