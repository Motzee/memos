<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domaines', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('id_domaine_parent')->nullable();
            $table->foreign('id_domaine_parent')->references('id')->on('domaines');

            $table->string('slug', 255)->unique() ;
            $table->string('label', 255) ;
            $table->string('type', 255)->nullable() ;            
            $table->text('comments')->nullable() ;
            $table->integer('numero')->nullable() ;
            
            $table->boolean('is_public')->default(1) ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domaines');
    }
}
