<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('references', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->unsignedBigInteger('id_memocateg')->nullable();
            $table->foreign('id_memocateg')->references('id')->on('memocategs');
            $table->unsignedBigInteger('id_memo')->nullable();
            $table->foreign('id_memo')->references('id')->on('memos');
            $table->string('label', 255) ;
            $table->string('url', 255)->nullable() ;
            $table->string('path_capture', 255)->nullable() ;
            $table->boolean('is_rss')->default(0) ;
            $table->boolean('is_veille')->default(0) ;
            $table->boolean('is_public')->default(1) ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('references');
    }
}
