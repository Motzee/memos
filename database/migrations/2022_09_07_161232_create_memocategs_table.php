<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemocategsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memocategs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->unsignedBigInteger('id_memocategs_parent')->nullable();
            $table->foreign('id_memocategs_parent')->references('id')->on('memocategs');
            
            $table->string('label', 255) ;
            $table->string('slug', 255) ;
            $table->string('domain', 255) ;
            $table->string('path', 255)->nullable() ;
            $table->integer('numero')->nullable() ;
            $table->boolean('is_public')->default(1) ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('memocategs', function (Blueprint $table) {
            $table->dropForeign('memocategs_id_user_foreign');
        });
        Schema::dropIfExists('memocategs');
    }
}
