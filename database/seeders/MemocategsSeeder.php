<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Memocateg;

class MemocategsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categsLangages = [
            1 => [
                'slug' => 'langages',
                'label' => 'Langages et syntaxes',
                'subcategs' => [
                    [
                        'slug' => 'php',
                        'label' => 'PHP'
                    ],
                    [

                        'slug' => 'js',
                        'label' => 'JS'
                    ],
                    [
                        'slug' => 'moteurs_de_templates',
                        'label' => 'Moteurs de templates',
                        'subcategs' => [
                            [
                                'slug' => 'twig',
                                'label' => 'Twig',
                            ],
                            [
                                'slug' => 'blade',
                                'label' => 'Blade',
                            ]
                        ]
                    ]
                ]
            ]
        ];
        //$this->insertArborescentCategs($categsLangages);


        $categsDonnees = [
            2 => [
                'slug' => 'donnees',
                'label' => 'Données',
                'subcategs' => [
                    [
                        'slug' => 'mysql',
                        'label' => 'MySQL (=MariaDB)'
                    ],
                    [
                        'slug' => 'orm',
                        'label' => 'ORM',
                        /*'subcategs' => [
                            [
                                'slug' => 'eloquent',
                                'label' => 'Eloquent'
                            ],
                            [
                                'slug' => 'doctrine',
                                'label' => 'Doctrine'
                            ]
                        ]*/
                    ]
                ]
            ]
        ];
        $this->insertArborescentCategs($categsDonnees);


        $categsFrameworks = [
            3 => [
                'slug' => 'frameworks',
                'label' => 'Frameworks',
                'subcategs' => [
                    [
                        'slug' => 'front',
                        'label' => 'Front (côté client)'
                    ],
                    [
                        'slug' => 'back',
                        'label' => 'Back (côté serveur)',
                        'subcategs' => [
                            [
                                'slug' => 'laravel',
                                'label' => 'Laravel'
                            ],
                            [
                                'slug' => 'symfony',
                                'label' => 'Symfony'
                            ]
                        ]
                    ]
                ]
            ]
        ];
        //$this->insertArborescentCategs($categsFrameworks);


        $categsEnv = [
            7 => [
                'slug' => 'environnement_de_dev',
                'label' => 'Environnement de développement',
                'subcategs' => [
                    [
                        /*'slug' => 'versionning',
                        'label' => 'Versionning',
                        'subcategs' => [
                            [*/
                                'slug' => 'git',
                                'label' => 'Git'
                            /*]
                        ]*/
                    ]
                ]
            ]
        ];
        $this->insertArborescentCategs($categsEnv);



    /* Insert Parent categs (in navbar)     Langages
     - HTML (accessibilité, référencement)
     - CSS (accessibilité, référencement, SASS, LESS)
     - JS (accessibilité, référencement, TypeScript)
     - PHP
     - Python
     - Ruby
     - markdown
     - moteurs de templates
     - Twig
     - Blade
     - Mustache
     
Données
     - SQL
     - Redis
     - XML
     - json
     
Frameworks
     - Bootstrap (CSS)
     - VueJS (JS)
     - Angular (JS)
     - React (JS)
     - Symfony (PHP)
     - Laravel (PHP)
     - Slim (PHP)
     - NodeJS (JS)
     
CMS et WYSIWYG
     - FileMaker
     - WordPress
     - Drupal
     
Libs
     
Divers du Metier
     - ergonomie des interfaces
     - graphismes
     - organisation de projet
     - sécurité
     
     
serveurs
     - LAMP/WAMP
     - NginX
     - rabbitMQ
     - Webdis (pour Reddis)
     
Environnement de dev :
     - OS
     - IDE
     - débugueurs
     - requêteurs
     - testeurs
     - versionning
     -git
     */
    }

    protected function insertArborescentCategs(array $arboCateg, $parentCategId = null)
    {
        foreach ($arboCateg as $key => $aCateg) {
            $nCateg = Memocateg::create([
                'id_user' => 1,
                'id_memocategs_parent' => $parentCategId,
                'label' => $aCateg['label'],
                'slug' => $aCateg['slug'],
                'domain' => 'inf_prog',
                'path' => $aCateg['slug'],
                'numero' => $key
            ]);
            if (array_key_exists('subcategs', $aCateg)) {
                $this->insertArborescentCategs($aCateg['subcategs'], $nCateg->id);
            }
        }
    }
}
