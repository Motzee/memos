<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\MemoSeeder;
use Illuminate\Support\Facades\DB;
use Database\Seeders\MemocategsSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('memos')->truncate();
        DB::table('memocategs')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call([
            MemocategsSeeder::class,
            MemoSeeder::class
        ]);
    }
}
