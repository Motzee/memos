<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Memocateg;

class MemoSeeder extends Seeder
{
    
    protected function insertInMemos(array $arrayOfMemos, string $slug) {
        $parentCateg = Memocateg::where('slug', $slug)->firstOrFail() ;
        foreach ($arrayOfMemos as $key => $aMemo) {
            DB::table('memos')->insert([
                'id_user' => 1,
                'id_memocategs'=> $parentCateg->id,
                'title' => $aMemo[0],
                'slug' => $aMemo[1],
                'path'=> 'inf_prog/'.$slug.'/'.$aMemo[2],
                'numero' => $key
            ]);
        }
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gitMemos = [
            [
                'Introduction à Git',
                'introduction',
                '00_intro'
            ],
            [
                'Suivre un projet avec git',
                'suivre_projet',
                '01_giter_projet'
            ],
            [
                'Actions de base de Git',
                'actions_basiques',
                '02_actions_bases'
            ],
            [
                'Système de branches parallèles',
                'branches_git',
                '03_branches'
            ],
            [
                'Gestion des erreurs avec Git',
                'gestion_erreurs',
                '04_erreurs'
            ],
            [
                'Sauvegarder et partager son projet avec un dépôt distant','depot_distant',
                '05_depot_distant'
            ],
            [    
                'Travailler en équipe avec Git',
                'travail_collaboratif',
                '06_travail_collaboratif'
            ],
            [
                'Automatiser les déploiements avec GitLab','automatiser_deploiements_gitlab',
                '09_automatiser_deploy'
            ],
            [
                'Utiliser une interface graphique comme intermédiaire pour Git','interface_graphique',
                '10_interfaces_graphiques'
            ]
        ] ;
        
        $sqlMemos = [
            [
                'Installation du client SQL MariaDB',
                'installer_mariadb',
                '00_intro_install_MariaDB'
            ],
            [
                'Gestion des utilisateurs SQL',
                'gestion_users',
                '01_users'
            ],
            [
                'Créer et gérer sa base de données',
                'gestion_db',
                '02_db'
            ],
            [
                'Créer et gérer ses tables',
                'gestion_tables',
                '03_table_crud'
            ],
            [
                'Interagir avec les données d\'une table',
                'gestion_donnees',
                '05_table_entries'
            ],
            [
                'Exploiter les relations entre tables : les jointures',
                'jointures',
                '04_table_joints'
            ],
            [
                'Optimiser la structure de sa BDD et ses requêtes',
                'optimisation',
                '10_optimization'
            ],
            [
                'Utiliser une interface graphique pour SQL',
                'interfaces_graphiques',
                '11_interfaces'
            ]
        ] ;

        $this->insertInMemos($gitMemos, 'git');
        $this->insertInMemos($sqlMemos, 'mysql');
    }
}
