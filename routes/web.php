<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemosController;
use App\Http\Controllers\VocabwordController;
use App\Http\Controllers\MemocategsController;
use App\Http\Controllers\ReferencesController;
use App\Models\Vocabword;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__ . '/auth.php';

Route::group([
    'middleware' => 'auth',
    'prefix' => 'admin'
], function () {
    /*Route::get('/categs', function ()    {
        dd("here");
    })->name('memocategs.index');*/
    Route::resources([
        'memos'         => MemosController::class,
        'memocategs'    => MemocategsController::class,
        'references'    => ReferencesController::class,
        'vocabwords'    => VocabwordController::class
    ]);
});

Route::get('/', [Controller::class, 'home'])->name('home');

Route::get('/vocabulaire', [VocabwordController::class, 'devine'])->name('vocab.devine');

//Présentation du domaine : le menu de nav permet d'accéder à des catégories
Route::get('/memos/{slugSubDomaine}', [Controller::class, 'subDomaineContent'])->where(['slugSubDomaine' => '[a-z0-9_\-]+'])->name('subdomain.index');

//la même route sert à accéder au détail des catégories et à celui des sous-catégories
Route::get('/memos/{slugSubDomaine}/{slugCateg}', [MemocategsController::class, 'categContent'])->where([
    'slugSubDomaine' => '[a-z0-9_\-]+',
    'slugCateg' => '[a-z0-9_\-]+',
])->name('memocategs.show2');



Route::get('/memos/{subDomain}/{subCateg}/{memo}', [MemosController::class, 'show'])->where([
    'subDomain' => '[a-z0-9_\-]+',
    'subCateg' => '[a-z0-9_\-]+',
    'memo' => '[a-z0-9_\-]+',
])->name('memos.show2');



/*

/*
Route::get('/', function () {
    return view('home');
})->middleware(['auth'])->name('home');*/
/*
Route::resources([
    'memos' => MemosController::class,
    'memocategs' => MemocategsController::class,
]);*//*
Route::middleware('auth')->group(function() {
    Route::resources([
        'references' => ReferencesController::class
    ]);
});

Route::resource('memos', MemosController::class)->parameters([
    ['memos' => 'slug']
]);

/*
Route::resource('memocategs', MemocategsController::class)->parameters([
    ['memocategs' => 'slug']
]);*/

